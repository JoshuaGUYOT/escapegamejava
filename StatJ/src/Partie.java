import java.util.Date;

public class Partie {

    private Integer idUtilisateur;
    private Integer idPartie;
    private Date datePartie;
//    private String dureeLimite;
    private String resultat;

    public Partie(Integer idUtilisateur, Integer idPartie, Date datePartie, String resultat){
        this.idUtilisateur = idUtilisateur;
        this.idPartie = idPartie;
        this.datePartie = datePartie;
//        this.dureeLimite = duree;
        this.resultat = resultat;
    }
//---------------[getters]---------------
    public Integer getIdUtilisateur() {
        return idUtilisateur;
    }

    public Integer getIdPartie() {
        return idPartie;
    }

    public Date getDatePartie() {
        return datePartie;
    }

//    public String getDureeLimite() {
//        return dureeLimite;
//    }

    public String getResultat() {
        return resultat;
    }
    //---------------------------------------
    @Override
    public boolean equals(Object o){
        if (o == null){ return false;}
        if (o instanceof Partie){
            Partie autrePartie = (Partie) o;
            return this.idPartie.equals(autrePartie.idPartie);
        }
        return false;
    }
}
