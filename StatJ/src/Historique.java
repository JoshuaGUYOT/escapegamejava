import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Historique {

    private int idJoueur;
    private List<Partie> historique;

    public Historique(int idJoueur){
        this.historique = new ArrayList<>();
        this.idJoueur = idJoueur;
    }

    public int getIdJoueur() {
        return idJoueur;
    }

    public List<Partie> getHistorique() {
        return historique;
    }

    public boolean MajHistorique(){

        boolean ajoute = false;
        LogingJDBC l = new LogingJDBC();
        Connection c = l.getConnexion();
        Statement ps = c.Statement();

        ResultSet rs = ps.executeQuery("select idPa, dateDebutPa, gagne " +
                                        "from PARTIE " +
                                        "where idUt = " + idJoueur +
                                        "order by dateDebutPa DESC");
        while (rs.next()){
            int idPa = rs.getInt("idPa");
            Date datePartie = rs.getDate("dateDebutPa");
            String resultat = rs.getString("gagne");
            Partie p = new Partie(idJoueur, idPa, datePartie, resultat);
            if (!this.historique.contains(p)){
                historique.add(p);
                ajoute = true;
            }
        }
        return ajoute;
    }
}