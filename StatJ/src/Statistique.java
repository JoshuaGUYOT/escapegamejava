import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Statistique extends Application {

    private int idJoueur;
    private Historique historique;
    private Label nombrePartiJouer;
    private Label tauxDeReussite;
    private Label victoirDefait;
    private Button retour;
    private Button  home;



    public ListView setTableauParties() {
        this.historique = new Historique(this.idJoueur);
        ListView<BorderPane> tableauParties = new ListView<>();
        for (Partie p : this.historique.getHistorique()){
            BorderPane partie = new BorderPane();
            partie.setLeft(new Label("Partie ; " + p.getIdPartie()));
            if (p.getResultat().equals("1")){
                partie.setRight(new Label("Victoire"));
                partie.setStyle("-fx-background-color: #b9fd9e; ");
            }
            else{
                partie.setRight(new Label("Défaite"));
                partie.setStyle("-fx-background-color: #fd9e9e; ");
            }
            tableauParties.getItems().add(partie);
        }
        return tableauParties;
    }

    public static void main(String[] args) {
        launch(args);
    }


    public BorderPane retourHome(){
        BorderPane bpEnTete = new BorderPane();
        bpEnTete.setStyle("-fx-background-color: #a06b56; ");
        Image img = new Image("file:./home.jpg",50,15,true,true);
        ImageView view = new ImageView(img);
        Image img2 = new Image("file:./retour.png",50,15,true,true);
        ImageView view2 = new ImageView(img2);
        this.retour = new Button();
        this.retour.setGraphic(view2);
        //this.retour.setOnAction(new BoutonStatique(this));
        this.retour.setPrefWidth(75);
        this.home = new Button();
        this.home.setGraphic(view);
        // this.home.setAlignment(Pos.CENTER_RIGHT);
        //this.home.setOnAction(new BoutonStatique(this));
        this.home.setPrefWidth(75);
        Label l= new Label("Statistique");
        l.setAlignment(Pos.CENTER);
        l.setStyle("-fx-font-weight: bold");
        l.setFont(new Font(l.getFont().getName(),20));
        l.setPrefWidth(130);
        bpEnTete.setLeft(this.retour);
        bpEnTete.setRight(this.home);
        bpEnTete.setCenter(l);
        return bpEnTete;
    }


    public VBox nbParties(){
        VBox vG = new VBox();
        vG.setStyle("-fx-background-color: #ce8a70;");
        vG.setPrefWidth(275);
        vG.setPrefHeight(150);
        vG.setSpacing(10);
        this.nombrePartiJouer = new Label("getNbParties(joueur)");
        vG.getChildren().addAll(this.nombrePartiJouer);
        return vG;
    }

    public VBox tauxReussite(){
        VBox vD = new VBox();
        vD.setAlignment(Pos.TOP_RIGHT);
        vD.setStyle("-fx-background-color: #ce8a70;");
        vD.setPrefWidth(275);
        vD.setPrefHeight(150);
        vD.setSpacing(10);
        this.tauxDeReussite = new Label("getTauxReussite(joueur)");
        this.victoirDefait = new Label("getVictoireDefaite(joueur)");
        vD.getChildren().addAll(this.tauxDeReussite, this.victoirDefait );
        return vD;
    }


    public BorderPane vGetvD(){
        BorderPane h = new BorderPane();
        h.setStyle("-fx-background-color: #ce8a70;");
        h.setPadding(new Insets(20, 20, 20,20 ));

        h.setRight(tauxReussite());
        h.setLeft(nbParties());
        return h;
    }



    public VBox historique() {
//        ----------[Entete]----------
        BorderPane entete = new BorderPane();
        Label lHisto = new Label("Historique des parties");
        Label lRes = new Label("Résultat");
        entete.setLeft(lHisto);
        entete.setRight(lRes);
//        ----------[Tableau parties]----------
        ScrollPane tableauParties = new ScrollPane();
        tableauParties.setContent(setTableauParties());
//        ----------[Historique]----------
        VBox historique = new VBox();
        historique.getChildren().add(entete);
        historique.getChildren().add(tableauParties);

        return historique;
    }

    public Scene pageStatistique() {
        BorderPane classement = new BorderPane();
        classement.setStyle("-fx-background-color: #ce8a70;");
        classement.setTop(retourHome());
        classement.setCenter(vGetvD());
        classement.setBottom(historique());
        return new Scene(classement, 600, 400);
    }


    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Statistique");
        primaryStage.setScene(pageStatistique());
        primaryStage.show();
    }

}