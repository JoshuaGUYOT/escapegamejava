import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.LightBase;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.paint.Color;

public class Enigme extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) {
        primaryStage.setTitle("Enigme");
        primaryStage.setScene(enigme());
        primaryStage.show();
    }
    private TextField rep;
    private Scene enigme() {
        Label title = new Label(" Nom Enigme ");
        title.setFont(new Font(16));
        title.setAlignment(Pos.CENTER);
        VBox titleBox = new VBox();
        titleBox.setAlignment(Pos.CENTER);
        titleBox.getChildren().addAll(title);

        Label rep = new Label("Réponse :");
        this.rep=new TextField();
        VBox buttonBox= new VBox();
        buttonBox.getChildren().addAll();
        buttonBox.setAlignment(Pos.CENTER_RIGHT);
        Button valider = new Button("Valider");
        Button indice = new Button("?");

        VBox reponse=new VBox();
        reponse.getChildren().addAll(this.rep);
        rep.setPadding(new Insets(10,5,10,5));
        
        Label texte = new Label("Texte Enigme");
        texte.setFont(new Font(14));
        VBox textBox = new VBox();
        textBox.getChildren().addAll(texte,rep,reponse);
        buttonBox.getChildren().addAll(valider,indice);
        buttonBox.setAlignment(Pos.CENTER_RIGHT);
        BorderPane bp = new BorderPane();

        bp.setBackground(new Background(new BackgroundFill(Color.rgb(205,162,142),null,null)));
        bp.setTop(titleBox);
        bp.setCenter(textBox);
        bp.setBottom(buttonBox);

        return new Scene(bp,500,180);
    }
    public TextField getRep(){return this.rep;}
}
