import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.paint.Color;

public class Indice extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) {
        primaryStage.setTitle("Indice");
        primaryStage.setScene(indice());
        primaryStage.show();
    }

    private Scene indice() {
        Label title = new Label(" INDICE ");
        title.setFont(new Font(16));
        title.setAlignment(Pos.CENTER);
        VBox titleBox = new VBox();
        titleBox.setAlignment(Pos.CENTER);
        titleBox.getChildren().addAll(title);
        //Button close = new Button("X");
        //close.setStyle("-fx-background-color: #BB0B00; ");
        //VBox buttonBox= new VBox();
        //buttonBox.getChildren().addAll(close);
        //buttonBox.setAlignment(Pos.TOP_RIGHT);


        Label texte = new Label("Texte indice");
        texte.setFont(new Font(14));
        BorderPane bp = new BorderPane();

        bp.setBackground(new Background(new BackgroundFill(Color.rgb(205,162,142),null,null)));
        bp.setTop(titleBox);
        //bp.setTop(buttonBox);
        bp.setCenter(texte);

        return new Scene(bp,500,140);
    }
}
