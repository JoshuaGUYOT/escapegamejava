import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class MonCompteEvent implements EventHandler<ActionEvent> {
    private PageAcceuilJoueur acceuilJoueur;
    private Utilisateur utilisateur;
    private PageAcceuil main;
    private MonCompte monComptePage;
    public MonCompteEvent(PageAcceuil main, PageAcceuilJoueur acceuilJoueur, Utilisateur utilisateur, MonCompte monComptePage){
        this.main = main;
        this.utilisateur = utilisateur;
        this.acceuilJoueur = acceuilJoueur;
        this.monComptePage = monComptePage;
    }
    @Override
    public void handle(ActionEvent e) {
        Button b = (Button) e.getTarget();
        if (b.getText().endsWith("rincipal")) {
            this.main.sceneChanger(acceuilJoueur.creerFormulaire());
        }
        else if (b.getText().endsWith("Possibles")) {
            ListeScenar listeScenar = new ListeScenar(this.main, this.utilisateur,this.monComptePage);
            this.main.sceneChanger(listeScenar.getSceneScenar());
        }
        else {
            Statistique listeScenar = new Statistique(this.utilisateur,this.monComptePage, this.main);
            this.main.sceneChanger(listeScenar.classement());
        }
    }
}
