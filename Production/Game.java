import javafx.scene.image.Image;
import javafx.scene.layout.*;

import java.util.Vector;
import java.util.List;
import java.util.ArrayList;
import javafx.scene.image.ImageView;

import java.sql.*;

public class Game extends VBox {
    private Placement placement;
    private double largeurTuile;
    private double hauteurTuile;
    private Pion pion;
    private Scenario scenario;
    private CompteARebours compteARebours;
    private int idScenario;
    private PageAcceuil main;
    private int place;
    private List<Carte> listeCarte;
    private JeuJDBC requetteJDBC;

    public Game(PageAcceuil main , int idScenario) {
        super();
        this.place =0;
        this.placement = null;
        this.main = main;
        this.idScenario = idScenario;
        this.listeCarte = new ArrayList<>();

        try{
            PageAcceuil vueAccueil = new PageAcceuil();
            this.requetteJDBC = new JeuJDBC();
            this.scenario = requetteJDBC.getScenarioById(this.idScenario);

            for(int idCarte: requetteJDBC.listeCarteById(idScenario)){
                List<Enigme> listEnigme = new ArrayList<>();
                Image img = requetteJDBC.tilesetImage(idCarte);
                this.placement = new Placement((int) img.getHeight()/32,(int) img.getWidth()/32);
                Carte carte = requetteJDBC.getCarteById(idCarte,this,img,listEnigme);

                for(Integer idEn: requetteJDBC.listeIdEnigmeForACarte(idCarte)){
                    Enigme enigme = requetteJDBC.getEnigmeById(idEn);
                    carte.add(enigme);
                }
                listeCarte.add(carte);
           }
        }catch(SQLException e){
          System.out.println("Probleme constructeur VueJeu :"+e.getMessage());
        }

        this.compteARebours = new CompteARebours(this.scenario.getTempsMax(),this, this.placement,"Temps :");
        this.compteARebours.start();
        listeCarte.get(this.place).getCarte().add(compteARebours,10,0);
        this.pion = new Pion(new ImageView( new Image("file:images/1.png")));
        listeCarte.get(this.place).ajouteJoueur(this.pion,5,5);
        this.getChildren().addAll(listeCarte.get(this.place).getCarte());

    }

    public Vector<Tuile> vecteurTuile(Image img){
      double hauteurImage = img.getHeight();
      double largeurImage = img.getWidth();
      double taille = largeurImage / this.placement.getLargeur();
      double ratio=1;
      Vector<Tuile> lesTuiles = new Vector<>();
      // creer des tuiles...
      for (int ligne = 0; ligne < this.placement.getHauteur(); ligne++){
          for(int colonne = 0; colonne < this.placement.getLargeur(); colonne++){
              Tuile tuile = new Tuile(false, taille, taille, ratio, null, ligne, colonne, img);
              lesTuiles.add(tuile);
          }
      }
      return lesTuiles;
    }

    public void update(String d){

      this.pion.deplacer(d);
      try{
        listeCarte.get(this.place).getCarte().add(this.pion.getView(),this.pion.getX(),this.pion.getY());
      }catch(Exception e){

      }
      try{
        Carte carte = listeCarte.get(this.place);
        int idCa = carte.getId();
        for(Enigme enigme: carte.getListeEnigme()){
          try{
            int idEn = enigme.getId();
            if(this.pion.getX() == requetteJDBC.lig(idCa,idEn) && this.pion.getY() == requetteJDBC.col(idCa,idEn)){
              if(!enigme.getReussi())
                new LesEnigmes(enigme);
            }
          }catch(SQLException e){
            System.out.println("pb");
          }
        }

        if(carte.finit()){
            this.place += 1;
            listeCarte.get(this.place).getCarte().add(compteARebours,9,0);
            listeCarte.get(this.place).ajouteJoueur(this.pion,5,9);
            this.pion.setY(9);
            this.pion.setX(5);
            this.getChildren().setAll(this.listeCarte.get(this.place).getCarte());

            main.sceneChanger(this.getScene());
        }
      }catch(Exception e){
          this.compteARebours.stop();
        // ECRAN WIN

      }




    }

    public Placement getPlacement(){
      return this.placement;
    }
}
