import java.util.Vector;

public class Placement {

    private Vector<Integer> tileset; // représentation du jeu sous la forme d'un vecteur d'entier
    private int hauteur; // le nombre de lignes
    private int largeur; // le nombre de colonnes

    /**
     * Constructeur du taquin indiquant le nombre de lignes et de colonnes de celui-ci
     * @param hauteur nombre de lignes
     * @param largeur nombre de colonnes
     */
    public Placement(int hauteur, int largeur){
        this.hauteur = hauteur;
        this.largeur = largeur;
        this.tileset=new Vector<Integer>(hauteur*largeur);
        for (int i=0;i<hauteur*largeur-1;i++){
            this.tileset.add(i);
        }
    }

    /**
     * @return le nombre de lignes du taquin
     */
    public int getHauteur() {
        return hauteur;
    }

    /**
     * @return le nombre de colonnes du taquin
     */
    public int getLargeur() {
        return largeur;
    }

    public void setLargeur(int largeur){
      this.largeur = largeur;
    }

    public void setHauteur(int hauteur){
      this.hauteur = hauteur;
    }

}
