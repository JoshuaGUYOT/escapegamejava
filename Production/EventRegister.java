import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class EventRegister implements EventHandler<MouseEvent> {
    private PageAcceuil main;
    private Connexion connexion;
    public EventRegister(PageAcceuil primaryStage, Connexion connexion){
        this.main = primaryStage;
        this.connexion = connexion;
    }
    @Override
    public void handle(MouseEvent e) {
        CreerCompte registerWindows = new CreerCompte(this.main, this.connexion);
        this.main.sceneChanger(registerWindows.getRegister());

    }

}
