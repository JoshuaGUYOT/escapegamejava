import javafx.event.ActionEvent;
import javafx.event.EventHandler;

public class BackToAccountEvent implements EventHandler<ActionEvent> {
    private MonCompte monComptePage;
    private PageAcceuil main;
    public BackToAccountEvent(MonCompte monComptePage, PageAcceuil main){
        this.monComptePage = monComptePage;
        this.main = main;
    }
    @Override
    public void handle(ActionEvent e) {
        this.main.sceneChanger(this.monComptePage.monCompte());

    }
}
