import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class EventMdpForget implements EventHandler<MouseEvent> {
    private PageAcceuil main;
    public EventMdpForget(PageAcceuil primaryStage){
        this.main = primaryStage;
    }
    @Override
    public void handle(MouseEvent e) {
            MdpOublie forgetWindows = new MdpOublie(this.main);
            this.main.sceneChanger(forgetWindows.mdpForget());

    }

}
