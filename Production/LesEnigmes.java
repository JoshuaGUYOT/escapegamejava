import javafx.geometry.Pos;
import javafx.scene.control.Alert;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import java.util.Vector;
import java.util.List;
import java.util.ArrayList;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.control.Button;
import javafx.scene.layout.FlowPane;
import javafx.scene.text.FontWeight;
import javafx.scene.paint.Color;

public class LesEnigmes {

  private TextField texteReponse;
  private Stage nw;

  public LesEnigmes( Enigme enigme){
    BorderPane pane = new BorderPane();
    Label titre = new Label(enigme.getNom());
    titre.setFont(Font.font(20));
    FlowPane fptitre = new FlowPane();
    fptitre.getChildren().add(titre);
    pane.setTop(fptitre);
    Label texteEnigme = new Label(enigme.getQuestion()+"\n");
    texteEnigme.setFont(Font.font(15));
    VBox box = new VBox();
    FlowPane fptexte = new FlowPane();
    fptexte.getChildren().add(texteEnigme);
    box.getChildren().add(fptexte);
    box.getChildren().add(new ImageView(enigme.getImage()));
    Label rep = new Label("Réponse: ");
    this.texteReponse = new TextField();
    VBox v = new VBox();
    FlowPane fp = new FlowPane();
    fp.getChildren().addAll(rep);
    v.getChildren().addAll(fp,this.texteReponse);
    pane.setBottom(v);
    pane.setCenter(box);
    Scene sc = new Scene(pane);
    this.nw = new Stage();
    this.nw.setScene(sc);
    this.nw.setTitle("Enigme");
    this.nw.setScene(sc);
    this.nw.show();
    this.texteReponse.requestFocus();
  }

  public String getReponse(){
    return this.texteReponse.getText();
  }

  public TextField getTextField(){
    return this.texteReponse;
  }

  public void close(){
    this.nw.close();
  }

}
