import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;


public class PageAcceuil extends Application {

    private Stage stage;

    public int hauteurFondEcran = 500;
    public int largeurFondEcran = 600;

    public static void main(String [] args) {launch(args);}

    public Scene creerFormulaire(){
//        ----------[creation item]----------
        BorderPane acceuil = new BorderPane();
        VBox vBoxCentre = new VBox();
        VBox vboxTop = new VBox();
//        ---
        Label titre = new Label("Échappée Belle");
        titre.setFont(new Font("Times New Roman", 50));
        titre.setTextFill(Color.WHITE);
//        ---
        Button bJouer = new Button("Jouer");
        bJouer.setPrefSize(400, 25);
        bJouer.setStyle("-fx-background-color: #696969; ");
        bJouer.setTextFill(Color.WHITE);
        bJouer.setFont(new Font(15));
        bJouer.setPadding(new Insets(5));

        Button bSeConnecter = new Button("Se connecter");
        bSeConnecter.setOnAction(new AcceuilEvent(this));
        bSeConnecter.setPrefSize(400, 25);
        bSeConnecter.setStyle("-fx-background-color: #000000; ");
        bSeConnecter.setTextFill(Color.WHITE);
        bSeConnecter.setFont(new Font(15));
        bSeConnecter.setPadding(new Insets(5));

        Button bQuitter = new Button("Quitter");
        bQuitter.setPrefSize(400, 25);
        bQuitter.setStyle("-fx-background-color: #000000; ");
        bQuitter.setTextFill(Color.WHITE);
        bQuitter.setFont(new Font(15));
        bQuitter.setOnAction(new QuitEvent(this));
        bQuitter.setPadding(new Insets(5));
//        ---
        Image fondEcran = new Image("./images/acceuilBackground.png",700, 0, true, true);
//        ----------[add item]----------
        vboxTop.getChildren().add(titre);
        vBoxCentre.getChildren().addAll(bJouer, bSeConnecter, bQuitter);
//        ---
        acceuil.setTop(vboxTop);
        acceuil.setCenter(vBoxCentre);
//        ----------[setting panel]----------
        acceuil.setBackground(new Background(new BackgroundImage(fondEcran,BackgroundRepeat.REPEAT,
                                                                            BackgroundRepeat.REPEAT,
                                                                            BackgroundPosition.DEFAULT,
                                                                            BackgroundSize.DEFAULT)));
        vboxTop.setAlignment(Pos.CENTER);
        vboxTop.setPadding(new Insets(50));
        vBoxCentre.setAlignment(Pos.BOTTOM_CENTER);
        vBoxCentre.setPadding(new Insets(50));
        vBoxCentre.setSpacing(15);
        return new Scene(acceuil, largeurFondEcran, hauteurFondEcran);
    }
    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("L'Échapée Belle");
        primaryStage.setScene(creerFormulaire());
        primaryStage.show();
        this.stage = primaryStage;
    }
    public void sceneChanger(Scene scene){
        this.stage.setScene(scene);
    }
    public void stop(){
        this.stage.hide();
    }
}
