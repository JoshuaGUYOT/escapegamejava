import javafx.event.EventHandler;
import javafx.scene.control.Button;

import javafx.event.ActionEvent;

public class ListeJoueurEvent implements EventHandler<ActionEvent> {
    private Utilisateur user;
    private PageAcceuil main;
    private PageAcceuilAdmin prec;
    public ListeJoueurEvent(Utilisateur user,PageAcceuil main, PageAcceuilAdmin prec){
        this.user = user;
        this.main = main;
        this.prec = prec;
    }
    @Override
    public void handle(ActionEvent e){
        Button b = (Button) e.getTarget();
        if (b.getText().endsWith("Activer")) {
            AdminGereJDBC adminTools = new AdminGereJDBC();
            try {
                adminTools.activer(this.user);
                ListeDesJoueurs update = new ListeDesJoueurs(this.prec,this.main);
                this.main.sceneChanger(update.getGererJoueur());
            }
            catch (Exception exec) {
                System.out.println(exec.getMessage());
            }
        }
        else if (b.getText().endsWith("Désactiver")) {
            AdminGereJDBC adminTools = new AdminGereJDBC();
            try {
                adminTools.desactiver(this.user);
                ListeDesJoueurs update = new ListeDesJoueurs(this.prec,this.main);
                this.main.sceneChanger(update.getGererJoueur());
            }
            catch (Exception exec){
                System.out.println(exec.getMessage());
            }
        }

    }

}
