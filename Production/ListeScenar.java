import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.paint.Color;

public class ListeScenar {
    private PageAcceuil main;
    private Utilisateur user;
    private MonCompte account;
    public ListeScenar(PageAcceuil main,Utilisateur user, MonCompte account){
        this.main = main;
        this.user = user;
        this.account = account;
    }
    public Scene getSceneScenar() {
        // Create a ScrollPane

        Button boutonRetour = new Button();
        Image img = new Image("file:./images/back.png",20.0,0.0,true,true);
        boutonRetour.setStyle("-fx-background-color: #cda28e; ");
        boutonRetour.setGraphic(new ImageView(img));
        boutonRetour.setOnAction( new BackToAccountEvent(this.account, this.main));

        BorderPane enTete = new BorderPane();
        enTete.setLeft(boutonRetour);
        Label lab2 = new Label("Liste des scénarios disponibles");
        enTete.setCenter(lab2);
        BorderPane B = new BorderPane();
        lab2.setPadding( new Insets(9));
        B.setTop(enTete);

        HBox boxRecherche = new HBox();
        TextField texteF = new TextField();
        texteF.setMinWidth(500);
        Button boutonRecherche = new Button("valider");
        boxRecherche.getChildren().addAll(texteF,boutonRecherche);


        ScrollPane scrollPane = new ScrollPane();
        B.setCenter(scrollPane);
        scrollPane.setPadding(new Insets(5));

        List<BorderPane>  listeBoutons= new ArrayList<>();

        GetScenarJDBC getScenar = new GetScenarJDBC(this.user);
        List<Scenario> listeScenar = getScenar.getListeScenar();

        int i;
        for(i = 0; i<=listeScenar.size()-1; i++){
            BorderPane bord  = new BorderPane();

            //bord.setPadding(20);
            if(i%2==0){bord.setBackground(new Background(new BackgroundFill(Color.rgb(205,162,142),null,null)));}
            else if(i%2 !=0){bord.setBackground(new Background(new BackgroundFill(Color.rgb(100,150,250),null,null)));}
            Label l = new Label(listeScenar.get(i).getNomScenario() );
            l.setAlignment(Pos.CENTER_LEFT);
            Label labeloo = new Label("Temps : 00:10:00");
            labeloo.setAlignment(Pos.CENTER_RIGHT);
            labeloo.setPadding(new Insets(20,20,20,260));
            bord.setRight(labeloo);
            bord.setLeft(l);
            bord.setMinWidth(500);
            bord.setMaxWidth(1500);
            listeBoutons.add(bord);

            bord.setOnMouseReleased(j -> bord.setBackground(new Background(new BackgroundFill(Color.rgb(100,150,250),null,null))));
        }

        VBox boxx = new VBox(boxRecherche);
        boxx.setSpacing(5);
        boxx.getChildren().addAll(listeBoutons);

        BorderPane borderVbox = new BorderPane();
        borderVbox.getChildren().add(boxx);
       // borderVbox.setM

        scrollPane.setContent(boxx);
        boxx.setAlignment(Pos.CENTER_LEFT);
        boxx.setSpacing(7);
        scrollPane.setFitToWidth(true);

        boxx.prefWidthProperty().bind(scrollPane.widthProperty());
        // Always show vertical scroll bar
        B.setBackground(new Background(new BackgroundFill(Color.rgb(205,162,142),null,null)));
        return new Scene(B, this.main.largeurFondEcran,this.main.hauteurFondEcran);
    }

}
