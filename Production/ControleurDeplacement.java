import javafx.event.EventHandler;
import javafx.scene.input.KeyEvent;

public class ControleurDeplacement implements EventHandler<KeyEvent> {
    private Placement mod; // modèle du jeu
    private Game game; // vue du jeu

    public ControleurDeplacement(Placement mod, Game game) {
        this.mod=mod;
        this.game = game;
    }

    @Override
    public void handle(KeyEvent keyEvent) {
      String touche = keyEvent.getCode().toString();
      if(touche.equals("UP") || touche.equals("DOWN") || touche.equals("RIGHT") || touche.equals("LEFT"))
        game.update(touche);
    }
}
