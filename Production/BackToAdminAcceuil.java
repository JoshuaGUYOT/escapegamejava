import javafx.event.EventHandler;
import javafx.scene.control.Button;

import javafx.event.ActionEvent;

public class BackToAdminAcceuil implements EventHandler<ActionEvent> {
    private PageAcceuilAdmin prec;
    private PageAcceuil main;
    public BackToAdminAcceuil(PageAcceuilAdmin prec, PageAcceuil main){
        this.prec = prec;
        this.main = main;
    }
    @Override
    public void handle(ActionEvent e){
        this.main.sceneChanger(this.prec.creerFormulaire());
    }

}
