import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;


public class PageAcceuilJoueur {
    private PageAcceuil main;
    private Utilisateur utilisateur;
    public PageAcceuilJoueur(PageAcceuil main, Utilisateur utilisateur){
        this.main = main;
        this.utilisateur = utilisateur;
    }

    public Scene creerFormulaire(){
//        ----------[creation item]----------
        BorderPane acceuil = new BorderPane();
        VBox vBoxCentre = new VBox();
        VBox vboxTop = new VBox();
//        ---
        Label titre = new Label("Échappée Belle");
        titre.setFont(new Font("Times New Roman", 50));
        titre.setTextFill(Color.WHITE);
//        ---
        Button bJouer = new Button("Jouer");
        bJouer.setPrefSize(400, 25);
        bJouer.setStyle("-fx-background-color: #000000; ");
        bJouer.setTextFill(Color.WHITE);
        bJouer.setFont(new Font(15));
        bJouer.setPadding(new Insets(5));
        bJouer.setOnAction(new AcceuilJoueurEvent(this.main,this,this.utilisateur));

        Button bSeConnecter = new Button("Mon compte");
        bSeConnecter.setPrefSize(400, 25);
        bSeConnecter.setStyle("-fx-background-color: #000000; ");
        bSeConnecter.setTextFill(Color.WHITE);
        bSeConnecter.setFont(new Font(15));
        bSeConnecter.setPadding(new Insets(5));
        bSeConnecter.setOnAction(new AcceuilJoueurEvent(this.main,this,this.utilisateur));

        Button bQuitter = new Button("Quitter");
        bQuitter.setPrefSize(400, 25);
        bQuitter.setStyle("-fx-background-color: #000000; ");
        bQuitter.setTextFill(Color.WHITE);
        bQuitter.setFont(new Font(15));
        bQuitter.setPadding(new Insets(5));
        bQuitter.setOnAction(new QuitEvent(this.main));
//        ---
        Image fondEcran = new Image("./images/acceuilBackground.png",700, 0, true, true);
//        ----------[add item]----------
        vboxTop.getChildren().add(titre);
        vBoxCentre.getChildren().addAll(bJouer, bSeConnecter, bQuitter);
//        ---
        acceuil.setTop(vboxTop);
        acceuil.setCenter(vBoxCentre);
//        ----------[setting panel]----------
        acceuil.setBackground(new Background(new BackgroundImage(fondEcran,BackgroundRepeat.REPEAT,
                                                                            BackgroundRepeat.REPEAT,
                                                                            BackgroundPosition.DEFAULT,
                                                                            BackgroundSize.DEFAULT)));
        vboxTop.setAlignment(Pos.CENTER);
        vboxTop.setPadding(new Insets(50));
        vBoxCentre.setAlignment(Pos.BOTTOM_CENTER);
        vBoxCentre.setPadding(new Insets(50));
        vBoxCentre.setSpacing(15);
        return new Scene(acceuil, this.main.largeurFondEcran, this.main.hauteurFondEcran);
    }

}
