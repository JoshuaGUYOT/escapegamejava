import javafx.scene.image.Image;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class GetScenarJDBC {
    Connection connection;
    Statement st;
    Utilisateur user;

    public GetScenarJDBC(Utilisateur user) {
        this.user = user;
        String login = "groupe31b";
        String mdp = "21@info!iuto31b";
        String serveur = "46.105.92.223";
        String bd = "db31b";
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver non trouvé");
            System.exit(1);
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + serveur + "/" + bd, login, mdp);
        } catch (
                SQLException ex) {
            System.out.println("Echec de connection " + ex.getMessage());
            System.exit(1);
        }
    }
    public List<Scenario> getListeScenar(){
        try {
            st = connection.createStatement();
            ResultSet rs = st.executeQuery("select idsc from PARTIE where idut  = '" + this.user.getIdUt() +"' ;");
            List<Integer> liste = new ArrayList<>();
            while ( rs.next()){
                liste.add(rs.getInt(1));
            }
            rs.close();

            st = connection.createStatement();
            ResultSet rs2 = st.executeQuery("select * from SCENARIO where brouillonsc ='N';");
            List<Scenario> res = new ArrayList<>();
            while (rs2.next()){
                if (!liste.contains(rs2.getInt(1))){
                    Image img;
                    if (rs2.getBlob(4) != null){
                        Blob test = rs2.getBlob(4);
                        InputStream in = test.getBinaryStream();
                        img = new Image(in);
                    }
                    else {
                        img = null;
                    }

                    Scenario scenario = new Scenario(rs2.getInt(1),rs2.getString(2),rs2.getString(3),img,rs2.getTime(5),rs2.getString(6));
                    res.add(scenario);
                }
            }
            rs2.close();
            return res;


        } catch (SQLException e) {
            System.out.println("Problème SQL " + e.getMessage()+ "\n");
        }
        return null;
    }
    public List<Scenario> getListeScenarAll(){
        try {
            st = connection.createStatement();
            ResultSet rs2 = st.executeQuery("select * from SCENARIO;");
            List<Scenario> res = new ArrayList<>();
            while (rs2.next()){

                    Image img;
                    if (rs2.getBlob(4) != null){
                        Blob test = rs2.getBlob(4);
                        InputStream in = test.getBinaryStream();
                        img = new Image(in);
                    }
                    else {
                        img = null;
                    }

                    Scenario scenario = new Scenario(rs2.getInt(1),rs2.getString(2),rs2.getString(3),img,rs2.getTime(5),rs2.getString(6));
                    res.add(scenario);

            }
            rs2.close();
            return res;


        } catch (SQLException e) {
            System.out.println("Problème SQL " + e.getMessage()+ "\n");
        }
        return null;
    }

}
