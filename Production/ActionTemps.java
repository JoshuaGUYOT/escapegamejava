import javafx.event.ActionEvent;
import javafx.event.EventHandler;

/**
 * Contrôleur du compte à rebours
 */
public class ActionTemps implements EventHandler<ActionEvent> {
    /**
     * temps enregistré lors du dernier événement
     */
    private long tempsPrec;

    /**
     * Vue du compte à rebours
     */
    private CompteARebours compteARebours;
    /**
     * Modele du jeu de taquin
     */
    private Game game;

    private Placement placement;
    /**
     * temps restant du compte à rebours: c'est un peu le modèle du compre à rebours
     */
    private long tempsRestant;

    /**
     * Constructeur du contrôleur du compte à rebours
     * notez que le modèle du compte à rebours est tellement simple
     * qu'il est inclus dans le contrôleur sous la forme de la variable tempsRestant
     * @param compteARebours Vue du compte à rebours
     * @param game Modèle du jeu de placement
     */
    ActionTemps (CompteARebours compteARebours, Game game, Placement placement, long tempsRestant){
        this.compteARebours=compteARebours;
        this.game = game;
        this.placement=placement;
        this.tempsPrec=-1;
        this.tempsRestant=tempsRestant;
    }

    /**
     * Actions à effectuer tous les pas de temps
     * essentiellement mesurer le temps écoulé depuis la dernière mesure
     * et mise à jour du temps restant
     * Il faut aussi indiquer à l'utilisateur quand le temps est écoulé
     * Il faut arreter le compte à rebours si l'utilisateur a gagné
     * @param actionEvent événement Action
     */
    @Override
    public void handle(ActionEvent actionEvent) {
        long tpsActuel=System.currentTimeMillis(); // recupération de l'heure système
        if (!(tempsPrec == -1)) {
            long tempsEcoulé = tpsActuel - tempsPrec;
            if ((tempsRestant - tempsEcoulé) < 0) {
                this.compteARebours.stop();
            } else {
                tempsRestant -= tempsEcoulé;
                this.compteARebours.setTime(tempsRestant);
            }
        }
        tempsPrec = tpsActuel;


    }

    /**
     * Reinitialise le temps restant (normalement dans le modèle)
     */
    public void reset(long tempsRestant){
        this.compteARebours.setTime(5000);
        this.tempsRestant = 5000;
        this.tempsPrec = -1;
    }
}