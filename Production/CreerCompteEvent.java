import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

public class CreerCompteEvent implements EventHandler<ActionEvent> {
    private Connexion main;
    private PageAcceuil acceuil;
    private CreerCompte registerPage;
    private String nomBouton;

    public CreerCompteEvent(Connexion primaryStage, PageAcceuil acceuil, CreerCompte registerPage,String nomBouton){
        this.main = primaryStage;
        this.acceuil = acceuil;
        this.registerPage = registerPage;
        this.nomBouton = nomBouton;
    }
    @Override
    public void handle(ActionEvent e) {
        Button b = (Button) e.getTarget();
        if (nomBouton.endsWith("Valider")) {
            try {
                if(this.registerPage.getEmail().getText().length() != 0 && this.registerPage.getMDP().getText().length() != 0 && this.registerPage.getMDP2().getText().length() != 0 && this.registerPage.getLogin().getText().length() != 0 && this.registerPage.getMDP2().getText().equals(this.registerPage.getMDP().getText())){
                    RegisterUtilisateurJDBC registerCheck = new RegisterUtilisateurJDBC();
                    if (registerCheck.register(this.registerPage.getLogin().getText(),this.registerPage.getMDP().getText(),this.registerPage.getEmail().getText())){
                        Alert al =new Alert(Alert.AlertType.INFORMATION);
                        al.setTitle("Bravo !");
                        al.setHeaderText("Vous êtes bien enregistré");
                        al.showAndWait();
                        this.acceuil.sceneChanger(this.main.getScene());
                    }
                    else {
                        Alert al = new Alert(Alert.AlertType.ERROR);
                        al.setTitle("Un utilisateur a déjà utilisé ces informations");
                        al.setHeaderText ("Un utilisateur a déjà utilisé ces informations");
                        al.showAndWait();
                    }

                }else{
                    if (!this.registerPage.getMDP2().getText().equals(this.registerPage.getMDP().getText())){
                        Alert al =new Alert(Alert.AlertType.ERROR);
                        al.setTitle("Les mot de passe ne corresponde pas.");
                        al.setHeaderText("Les mot de passe ne corresponde pas.");
                        al.showAndWait();
                    }
                    else{

                        Alert al =new Alert(Alert.AlertType.ERROR);
                        al.setTitle("Attention !");
                        al.setHeaderText("Pour envoyer vous créer un compte merci de remplir tout les champs.");
                        al.showAndWait ();
                    }
                }

            }
            catch (Exception exception){
                Alert al =new Alert(Alert.AlertType.ERROR);
                al.setTitle("Attention !");
                al.setHeaderText("Pour envoyer un email, rentré une adresse email.");
                al.setContentText("Champ email vide.");
                al.showAndWait ();
            }
        }
        else if(nomBouton.endsWith("Retour")){
            this.acceuil.sceneChanger(this.main.getScene());
        }
    }
}