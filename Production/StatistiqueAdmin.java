import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.ScrollPane;

import java.util.ArrayList;
import java.util.List;


public class StatistiqueAdmin   {
    private PageAcceuil main;
    private PageAcceuilAdmin prec;
    public StatistiqueAdmin(PageAcceuil main,PageAcceuilAdmin prec){
        this.main = main;
        this.prec = prec;
    }

    public BorderPane retourHome(){
        BorderPane bpEnTete = new BorderPane();
        bpEnTete.setStyle("-fx-background-color: #a06b56; ");
        Image img2 = new Image("file:./images/retour.png",50,15,true,true);
        ImageView view2 = new ImageView(img2);
        Button retour = new Button();
        retour.setGraphic(view2);
        retour.setOnAction(new BackToAdminAcceuil(this.prec,this.main));
        retour.setPrefWidth(75);
        Label l= new Label("Statistique");
        l.setAlignment(Pos.CENTER);
        l.setStyle("-fx-font-weight: bold");
        l.setFont(new Font(l.getFont().getName(),20));
        l.setPrefWidth(130);
        bpEnTete.setLeft(retour);
        bpEnTete.setCenter(l);
        return bpEnTete;
    }

    public VBox nbPartiesEnC(){
        VBox vbox = new VBox();
        vbox.setPadding(new Insets(10, 10, 10, 10));
        vbox.setAlignment(Pos.CENTER);
        vbox.setSpacing(10);
        vbox.setStyle("-fx-background-color: #f7c7b5;");
        Label l = new Label("Nombre de ");
        StatStatistique stat = new StatStatistique();
        Label l1 = new Label("parties :ERREUR");
        try{
            l1 = new Label("parties :" + stat.getNbPartiesAll());
        }
        catch ( Exception e){
            System.out.println("Pb SQL getNbPartiesAll()");
        }
        vbox.getChildren().addAll(l,l1);
        return vbox;
    }

    public VBox vG(){
        VBox v = new VBox();
        v.setPadding(new Insets(10, 10, 10, 10));
        v.setStyle("-fx-background-color: #ffffff;");
        v.getChildren().addAll(nbPartiesEnC());
        return v;
    }





    public VBox vboxDate(){
        VBox vD = new VBox();
        vD.setPadding(new Insets(10, 10, 10, 10));
        vD.setSpacing(10);
        vD.setAlignment(Pos.TOP_RIGHT);
        vD.setStyle("-fx-background-color: #f7c7b5;");
        Label l1 = new Label("Nombre de partie joués du");
        HBox h = new HBox();
        Label date = new Label("01/06 a Aujourd'hui : ERREUR");
        StatStatistique stat = new StatStatistique();
        try{
            date = new Label("01/06 a Aujourd'hui : " + stat.getNbPartiesAll());
        }
        catch ( Exception e){
            System.out.println("Pb SQL getNbPartiesAll()");
        }

        h.getChildren().addAll(date);
        vD.getChildren().addAll(l1, h);
        return vD;
    }


    public VBox vD(){
        VBox vD = new VBox();
        vD.setPadding(new Insets(10, 10, 10, 10));
        vD.setStyle("-fx-background-color: #ffffff;");
        vD.setSpacing(10);
        vD.getChildren().addAll(vboxDate());
        return vD;
    }


    public BorderPane vGetvD(){
        BorderPane h = new BorderPane();
        h.setStyle("-fx-background-color: #ce8a70;");
        h.setPadding(new Insets(20, 50, 20,50 ));
        h.setRight(vD());
        h.setLeft(vG());
        return h;
    }



    public ScrollPane tableau() {
        ScrollPane scrollPane = new ScrollPane();
        VBox tab= new VBox();
        scrollPane.setContent(tab);
        tab.setPrefWidth(500);
        tab.setPrefHeight(150);
        tab.setPadding(new Insets(10,50,20,50));
        tab.setStyle("-fx-background-color: #ffffff;");
        tab.setAlignment(Pos.CENTER);
        tab.setPadding(new Insets(0, 100, 40, 100));
        tab.setSpacing(10);
        BorderPane bp = new BorderPane();
        Label l = new Label("Liste des Scénarios/taux de réussite");
        bp.setLeft(l);
        tab.getChildren().add(bp);
        GetScenarJDBC getListeScenar = new GetScenarJDBC(this.prec.getAdmin());
        List<Scenario> listScenar = getListeScenar.getListeScenarAll();
        for (Scenario elem: listScenar){
            HBox x = new HBox();
            x.setStyle("-fx-background-color: #f7c7b5;");
            StatStatistique statWin = new StatStatistique();
            try {
                x.getChildren().add(new Label(elem.getNomScenario()+"\n"+statWin.getTauxReussiteScenar(elem)));
            }
            catch (Exception e){
                System.out.println("pb getTauxDeReussiteScenar");
            }
            tab.getChildren().add(x);

        }
        scrollPane.setFitToWidth(true);
        return scrollPane;
    }
    

    public BorderPane piedDePage(){
        BorderPane p = new BorderPane();
        p.setStyle("-fx-background-color: #ce8a70;");
        p.setCenter(tableau());
        p.setPadding(new Insets(10,50,20,50));
        return p;
    }

    public Scene classement() {
        BorderPane classement = new BorderPane();
        classement.setStyle("-fx-background-color: #ce8a70;");
        classement.setTop(retourHome());
        classement.setCenter(vGetvD());
        classement.setBottom(piedDePage());
        return new Scene(classement, this.main.largeurFondEcran, this.main.hauteurFondEcran);
    }


}