import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;

public class QuitEvent implements EventHandler<ActionEvent> {
    private PageAcceuil main;
    public QuitEvent(PageAcceuil primaryStage){
        this.main = primaryStage;
    }
    @Override
    public void handle(ActionEvent e) {
        this.main.stop();

    }

}
