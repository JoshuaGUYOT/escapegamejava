import javafx.scene.image.Image;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class AdminGereJDBC {
    Connection connection;
    Statement st;

    public AdminGereJDBC() {
        String login = "groupe31b";
        String mdp = "21@info!iuto31b";
        String serveur = "46.105.92.223";
        String bd = "db31b";
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver non trouvé");
            System.exit(1);
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + serveur + "/" + bd, login, mdp);
        } catch (
                SQLException ex) {
            System.out.println("Echec de connection " + ex.getMessage());
            System.exit(1);
        }
    }

    public List<Utilisateur> getListeJoueur() throws Exception {
        try {
            st = connection.createStatement();
            ResultSet rs = st.executeQuery("select * from UTILISATEUR;");
            List<Utilisateur> res = new ArrayList<>();
            while (rs.next()) {
                Image img = null;
                boolean active = false;
                if (rs.getString(5).equals("O")){
                    active = true;
                }
                res.add(new Utilisateur(rs.getInt(1),rs.getString(4),rs.getString(3),rs.getString(2),img,rs.getString(7),active));
            }
            rs.close();
            return res;

        } catch (SQLException throwables) {
            System.out.println("Problème SQL \n" + throwables.getMessage());
        }
        return null;
    }
    public void activer(Utilisateur user) throws Exception {
        try {
            st = connection.createStatement();
            st.executeQuery("UPDATE UTILISATEUR SET activeut='O' where idut = "+user.getIdUt()+";");

        } catch (SQLException throwables) {
            System.out.println("Problème SQL \n" + throwables.getMessage());
        }

    }

    public void desactiver(Utilisateur user) throws Exception {
        try {
            st = connection.createStatement();
            st.executeQuery("UPDATE UTILISATEUR SET activeut='N' where idut = "+user.getIdUt()+";");

        } catch (SQLException throwables) {
            System.out.println("Problème SQL \n" + throwables.getMessage());
        }
    }
}

