import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

import java.io.File;

public class LoginEvent implements EventHandler<ActionEvent> {
    private Connexion main;
    private PageAcceuil acceuil;
    private String nomBouton;

    public LoginEvent(Connexion primaryStage, PageAcceuil acceuil,String nomBouton){
        this.main = primaryStage;
        this.acceuil = acceuil;
        this.nomBouton = nomBouton;
    }
    @Override
    public void handle(ActionEvent e) {
        Button b = (Button) e.getTarget();
        if (nomBouton.endsWith("Valider")) {
            try {
                if (this.main.getLogin().getText().length() != 0 && this.main.getMDP().getText().length() != 0) {
                    LoginUtilisateurJDBC loginConnexion = new LoginUtilisateurJDBC();
                    if (loginConnexion.login(this.main.getLogin().getText(),this.main.getMDP().getText())){
                        Alert al =new Alert(Alert.AlertType.INFORMATION);
                        al.setTitle("Vous êtes connecté.");
                        al.setHeaderText("Vous êtes bien connecté.");
                        al.showAndWait();
                        GetUserJDBC JDBCgetteur = new GetUserJDBC();
                        Utilisateur utilisateur = JDBCgetteur.getUtilisateur(this.main.getLogin().getText(), this.main.getMDP().getText());
                        PageAcceuilJoueur page1 = new PageAcceuilJoueur(this.acceuil,utilisateur);
                        this.acceuil.sceneChanger(page1.creerFormulaire());
                        if (utilisateur.getRole().equals("1")) {
                            PageAcceuilAdmin page2 = new PageAcceuilAdmin(this.acceuil, utilisateur);
                            this.acceuil.sceneChanger(page2.creerFormulaire());
                        }

                    }
                    else {
                        Alert al =new Alert(Alert.AlertType.ERROR);
                        al.setTitle("Erreur !");
                        al.setHeaderText("Mot de passe ou identifiant incorrect\n (votre compte est peu-être désactiver)");
                        al.showAndWait();
                    }


                }
                else {
                    this.main.getLogin().requestFocus();
                    Alert al =new Alert(Alert.AlertType.ERROR);
                    al.setTitle("Erreur !");
                    al.setHeaderText("Pour envoyer un email, rentré une adresse email.");
                    al.setContentText("Champ email vide.");
                    al.showAndWait ();
                }
            }
            catch (Exception exec){
                this.main.getLogin().requestFocus();
                Alert al =new Alert(Alert.AlertType.ERROR);
                al.setTitle("Attention !");
                al.setHeaderText("Pour envoyer un email, rentré une adresse email.");
                al.setContentText("Champ email vide.");
                al.showAndWait ();
            }
        }
        else{
            this.acceuil.sceneChanger(this.acceuil.creerFormulaire());
        }
    }
}