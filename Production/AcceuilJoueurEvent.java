import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;

public class AcceuilJoueurEvent implements EventHandler<ActionEvent> {
    private PageAcceuilJoueur acceuilJoueur;
    private Utilisateur utilisateur;
    private PageAcceuil main;
    public AcceuilJoueurEvent(PageAcceuil main, PageAcceuilJoueur acceuilJoueur, Utilisateur utilisateur){
        this.main = main;
        this.utilisateur = utilisateur;
        this.acceuilJoueur = acceuilJoueur;
    }
    @Override
    public void handle(ActionEvent e) {
        Button b = (Button) e.getTarget();
        if (b.getText().endsWith("compte")) {
            GetUserJDBC getUser = new GetUserJDBC();
            MonCompte accountPage = new MonCompte(main, acceuilJoueur, getUser.refreshUser(utilisateur));
            this.main.sceneChanger(accountPage.monCompte());
        }
        else {
            Game windowsGame = new Game(this.main,1);
            Scene game = new Scene(windowsGame,this.main.largeurFondEcran,this.main.hauteurFondEcran);
            this.main.sceneChanger(game);
            game.setOnKeyPressed(new ControleurDeplacement(windowsGame.getPlacement(),windowsGame));
        }
    }
}
