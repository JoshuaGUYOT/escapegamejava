import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;


public class Connexion {
    private VBox pane;
    private TextField id;
    private PasswordField mdp;
    private PageAcceuil main;
    private Scene scene;
    public Connexion(PageAcceuil main){
        this.main = main;
        this.id = new TextField();
        this.mdp = new PasswordField();
        this.pane = new VBox();
        this.scene = null;
    }

    public Scene connexionScreen() {
        pane.setStyle("-fx-background-color: #CCA28E");
        VBox vBox = new VBox();
        vBox.setPadding(new Insets(20,20,20,20));
        vBox.setSpacing(8);

        Button back = new Button();
        Image img = new Image("./images/retour.png",30,0,true,true);
        back.setStyle("-fx-background-color: #CCA28E");
        back.setGraphic(new ImageView(img));
        back.setOnAction(new LoginEvent(this,this.main,"Retour"));
        back.setPrefWidth(30);


        Label mdpForgetLink = new Label("Mot de passe oublié ?");
        mdpForgetLink.setTextFill(Color.web("0000FF",0.8));
        mdpForgetLink.setStyle("-fx-underline: true;");
        mdpForgetLink.setOnMouseClicked(new EventMdpForget(this.main));


        VBox titleB = new VBox();
        Label title = new Label("Se connecter");
        Label sousTitle = new Label("Créer un nouveau compte");


        sousTitle.setTextFill(Color.web("0000FF",0.8));
        sousTitle.setStyle("-fx-underline: true;");
        sousTitle.setOnMouseClicked(new EventRegister(this.main,this));


        VBox sousTitleB = new VBox();
        sousTitleB.getChildren().add(sousTitle);
        sousTitleB.setAlignment(Pos.CENTER);

        Button valider = new Button();
        Image img1 = new Image("./images/done.png",30,0,true,true);
        valider.setGraphic(new ImageView(img1));
        valider.setStyle("-fx-background-color:#CCA28E");

        id.setStyle("-fx-control-inner-background : #FFDAD0 ");
        mdp.setStyle("-fx-control-inner-background : #FFDAD0 ");

        HBox boutonValider = new HBox();
        boutonValider.getChildren().add(valider);
        boutonValider.setAlignment(Pos.BOTTOM_RIGHT);


        valider.setOnAction(new LoginEvent(this,this.main,"Valider"));



        title.setFont(new Font(title.getFont().getName(),20));
        title.setStyle("-fx-font-weight: bold");
        titleB.setAlignment(Pos.CENTER);
        titleB.setPadding( new Insets(10));
        titleB.getChildren().add(title);


        vBox.getChildren().addAll(back,
                titleB,
                sousTitleB,
                new Label("Identifiant"),
                this.id,
                new Label("Mot de passe"),
                this.mdp,
                mdpForgetLink);
        pane.getChildren().addAll(vBox,boutonValider);

        this.scene = new Scene(pane,this.main.largeurFondEcran,this.main.hauteurFondEcran);
        return this.scene;


    }
    public Scene getScene(){return this.scene;}
    public PasswordField getMDP(){return this.mdp;}
    public TextField getLogin() {return this.id;}
}
