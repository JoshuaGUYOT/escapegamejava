import javafx.scene.image.Image;

import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class JeuJDBC {

    Connection connection;
    Statement st;
    Utilisateur user;
    public JeuJDBC(){
        String login = "groupe31b";
        String mdp = "21@info!iuto31b";
        String serveur = "46.105.92.223";
        String bd = "db31b";
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver non trouvé");
            System.exit(1);
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + serveur + "/" + bd, login, mdp);
        } catch (
                SQLException ex) {
            System.out.println("Echec de connection " + ex.getMessage());
            System.exit(1);
        }
    }
    public Scenario getScenarioById(int idsc) throws SQLException{
        st = connection.createStatement();
        ResultSet rs = st.executeQuery("select * from SCENARIO where idsc = " + idsc + ";");
        rs.next();
        Image img = null;
        Blob test = rs.getBlob(4);
        if (!(test == null)){
            InputStream in = test.getBinaryStream();
            img = new Image(in);
        }

        return new Scenario(rs.getInt(1),rs.getString(2),rs.getString(3),img,rs.getTime(5),rs.getDate(6).toString());
    }
    public List<Integer> listeCarteById(int idsc) throws SQLException{
        List<Integer> res =new ArrayList<>();
        st = connection.createStatement();
        ResultSet rs = st.executeQuery("select idca, numordre from PARTICIPER where idSc="+idsc+" order by numordre;");
        while (rs.next()){
            res.add(rs.getInt("idca"));
        }
        rs.close();
        return res;
    }
    public Image tilesetImage(int idca) throws SQLException{
        st = connection.createStatement();
        ResultSet rs = st.executeQuery("select imagets from CARTE natural join TILESET where idca="+idca+";");
        rs.next();
        Image img = null;
        if (!(rs.getBlob(1) == null)){
            Blob test = rs.getBlob(1);
            InputStream in = test.getBinaryStream();
            img = new Image(in);
        }
        return img;
    }
    public String nomCarteById(int idca) throws SQLException{
        st = connection.createStatement();
        ResultSet rs = st.executeQuery("select * from CARTE where idCa="+idca+";");
        rs.next();
        return rs.getString(2);
    }
    public Carte getCarteById(int idCa, Game main, Image img, List<Enigme> listeEnigme) throws SQLException{
        st = connection.createStatement();
        ResultSet rs = st.executeQuery("select * from CARTE where idCa="+idCa+";");
        rs.next();
        return new Carte(rs.getInt(1),rs.getString(2),rs.getString(3),main.vecteurTuile(img),listeEnigme);

    }
    public List<Integer> listeIdEnigmeForACarte(int idca) throws SQLException{
        List<Integer> res = new ArrayList<>();
        st = connection.createStatement();
        ResultSet rs = st.executeQuery("select idEn from SITUER where idCa="+idca+";");
        while(rs.next()){
            res.add(rs.getInt("idEn"));
        }
        rs.close();
        return res;
    }
    public Enigme getEnigmeById(int idE) throws  SQLException{
        st = connection.createStatement();
        ResultSet rs = st.executeQuery("select * from ENIGME where iden="+idE+";");
        rs.next();
        Image img = null;
        if (!(rs.getBlob(4) == null)){
            Blob test = rs.getBlob(4);
            InputStream in = test.getBinaryStream();
            img = new Image(in);
        }
        boolean bool = true;
        if (rs.getString(7).equals("N")){
            bool = false;
        }
        return new Enigme(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getString(5),img,rs.getString(6),bool);
    }

    public int lig(int idca, int iden) throws SQLException{
        Statement st;
        st = connection.createStatement();
        ResultSet rs = st.executeQuery("select lig from SITUER where idEn="+iden+" and idCa="+idca);
        rs.next();
        int res = rs.getInt("lig");
        rs.close();
        return res;
    }

    public int col(int idca, int iden) throws SQLException{
        Statement st;
        st = connection.createStatement();
        ResultSet rs = st.executeQuery("select col from SITUER where idEn="+iden+" and idCa="+idca);
        rs.next();
        int res = rs.getInt("col");
        rs.close();
        return res;
    }
}
