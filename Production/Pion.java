import javafx.scene.image.ImageView;

public class Pion{

  private ImageView image;
  private int x;
  private int y;

  public Pion(ImageView image){
      this.image = image;
      this.image.setFitWidth(32);
      this.image.setFitHeight(32);
      this.x = 5;
      this.y = 5;
  }

  public ImageView getView(){
    return this.image;
  }

  public int getX(){
    return this.x;
  }

  public int getY(){
    return this.y;
  }

  public void deplacer(String st){
    if(st.equals("UP") && this.y > 0 )
      this.y -= 1;
    else if(st.equals("LEFT") && this.x > 0)
      this.x -= 1;
    else if(st.equals("RIGHT") && this.x < 9)
      this.x +=1;
    else if(st.equals("DOWN") && this.y < 9)
      this.y  += 1;
  }

  public void setX(int x){
    this.x = x;
  }

  public void setY(int y){
    this.y = y;
  }

}
