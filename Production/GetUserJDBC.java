import javafx.scene.image.Image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.sql.*;

public class GetUserJDBC {
    Connection connection;
    Statement st;
    private String logUt, mdpUt;

    public GetUserJDBC() {
        String login = "groupe31b";
        String mdp = "21@info!iuto31b";
        String serveur = "46.105.92.223";
        String bd = "db31b";
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver non trouvé");
            System.exit(1);
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + serveur + "/" + bd, login, mdp);
        } catch (
                SQLException ex) {
            System.out.println("Echec de connection " + ex.getMessage());
            System.exit(1);
        }
    }

    public Utilisateur getUtilisateur(String idU, String mdpU) {
        try {
            st = connection.createStatement();
            ResultSet rs = st.executeQuery("select * from UTILISATEUR where pseudout = '" + idU + "' and mdput = '" + mdpU + "';");
            rs.next();

            //On tranforme le Blob en Image
            Image img;
            if (!rs.getBlob(6).equals(null)){
                Blob test = rs.getBlob(6);
                InputStream in = test.getBinaryStream();
                img = new Image(in);
            }
            else {
                img = new Image("file:./images/index.png");
            }
            //On créé un Utilisateru avec les info de la BD
            return new Utilisateur(rs.getInt(1),rs.getString(4),rs.getString(3),rs.getString(2),img,rs.getString(7),rs.getBoolean(5));

        } catch (SQLException throwables) {
            System.out.println("Problème SQL \n" + throwables.getMessage());
        }
        return null;
    }
    public Utilisateur refreshUser(Utilisateur user) {
        try {
            st = connection.createStatement();
            ResultSet rs2 = st.executeQuery("select * from UTILISATEUR where pseudout = '" + user.getPseudo() + "' and mdput = '" + user.getMdp() + "';");
            rs2.next();
                //On tranforme le Blob en Image
                Image img;
                if (rs2.getBlob(6) != null){
                    Blob test = rs2.getBlob(6);
                    InputStream in = test.getBinaryStream();
                    img = new Image(in);
                }
                else {img = new Image("file:./images/index.png"); }
                //On créé un Utilisateru avec les info de la BD
            user.setImage(img);


            return user;

        } catch (SQLException throwables) {
            System.out.println("Problème SQL \n" + throwables.getMessage());
        }
        return null;
    }
}