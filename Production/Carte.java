import java.util.List;
import javafx.scene.layout.*;
import java.util.Vector;

public class Carte{

  private String nom;
  private int id;
  private String plan;
  private GridPane carte;
  private List<Enigme> listeEnigme;

  public Carte(int id, String nom, String plan,Vector<Tuile> lesTuiles, List<Enigme> listeEnigme){
    this.nom = nom;
    this.plan = plan;
    this.listeEnigme = listeEnigme;
    this.id = id;
    List<List<Integer>> listeTuile = Tuile.stringToList(plan);
    this.carte = new GridPane();
    for(int i = 0; i < listeTuile.size(); i++){
      for(int y = 0; y <listeTuile.get(i).size(); y++){
        this.carte.add(lesTuiles.get(listeTuile.get(i).get(y)).clone(),y,i);
      }
    }
  }

  public void add(Enigme enigme){
    this.listeEnigme.add(enigme);
  }

  public int getId(){
    return this.id;
  }

  public void ajouteJoueur(Pion pion,int x, int y){
    this.carte.add(pion.getView(),5,5);
  }

  public GridPane getCarte(){
    return this.carte;
  }

  public List<Enigme> getListeEnigme(){
    return this.listeEnigme;
  }

  public boolean finit(){
    for(Enigme enigme: listeEnigme){
      if(!enigme.getReussi()){
        return false;
      }
    }
    return true;
  }

}
