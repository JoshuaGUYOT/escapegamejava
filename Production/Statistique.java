import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;

import java.util.concurrent.ExecutionException;

public class Statistique  {



    private ListView<HBox> tableauParties;
    private Label nombrePartiJouer;
    private Label tauxDeReussite;
    private Label victoirDefait;
    private Button retour;
    private Historique historique;
    private Utilisateur user;
    private MonCompte compte;
    private PageAcceuil main;


    public Statistique(Utilisateur user,MonCompte compte,PageAcceuil main){
        this.user = user;
        this.compte = compte;
        this.main = main;
    }






    public void setTableauParties() {

        this.historique = new Historique(this.user.getIdUt());
        for (Partie x: this.historique.majHistorique()) {
            if (x.getResultat() == "1") {
                HBox y = new HBox();
                y.getChildren().addAll(new Label("" + x.getIdPartie()));
                y.setStyle("-fx-background-color: #b9fd9e; ");
                tableauParties.getItems().add(y);
            } else {
                HBox z = new HBox();
                z.getChildren().addAll(new Label("" + x.getIdPartie()));
                z.setStyle("-fx-background-color: #fd9e9e; ");
                tableauParties.getItems().add(z);
            }
        }
    }

    public BorderPane retourHome(){
        BorderPane bpEnTete = new BorderPane();
        bpEnTete.setStyle("-fx-background-color: #a06b56; ");
        Image img2 = new Image("file:./images/retour.png",50,15,true,true);
        ImageView view2 = new ImageView(img2);
        this.retour = new Button();
        this.retour.setGraphic(view2);
        this.retour.setOnAction(new BackToAccountEvent(this.compte,this.main));
        this.retour.setPrefWidth(75);
        Label l= new Label("Statistiques");
        l.setAlignment(Pos.CENTER);
        l.setStyle("-fx-font-weight: bold");
        l.setFont(new Font(l.getFont().getName(),20));
        l.setPrefWidth(139);
        bpEnTete.setLeft(this.retour);
        bpEnTete.setCenter(l);
        return bpEnTete;
    }


    public VBox nbParties(){
        VBox vG = new VBox();
        vG.setStyle("-fx-background-color: #ce8a70;");
        vG.setPrefWidth(275);
        vG.setPrefHeight(150);
        vG.setSpacing(10);
        try {
            this.nombrePartiJouer = new Label(this.user.getPseudo());
        }
        catch (Exception e) {
            System.out.println("SQL exception");
        }
        vG.getChildren().addAll(this.nombrePartiJouer);
        return vG;
    }

    public VBox tauxReussite(){
        StatStatistique stat = new StatStatistique();
        VBox vD = new VBox();
        vD.setAlignment(Pos.TOP_RIGHT);
        vD.setStyle("-fx-background-color: #ce8a70;");
        vD.setPrefWidth(275);
        vD.setPrefHeight(150);
        vD.setSpacing(10);
        try {
            this.tauxDeReussite = new Label(stat.getTauxReussite(this.user.getPseudo()));
        }
        catch (Exception e){
            System.out.println("Pb SQL1"+e.getMessage());
        }
        try {
            this.victoirDefait = new Label(stat.getVictoireDefaite(this.user.getPseudo()));
        }
        catch (Exception e){
            System.out.println("Pb SQL2");
        }
        vD.getChildren().addAll(this.tauxDeReussite, this.victoirDefait );
        return vD;
    }


    public BorderPane vGetvD(){
        BorderPane h = new BorderPane();
        h.setStyle("-fx-background-color: #ce8a70;");
        h.setPadding(new Insets(20, 20, 20,20 ));

        h.setRight(tauxReussite());
        h.setLeft(nbParties());
        return h;
    }



    public VBox tableau() {
        VBox tab= new VBox();
        //tab.setStyle("-fx-background-color: #ce8a70;");
        tab.setAlignment(Pos.CENTER);
        tab.setPadding(new Insets(0, 100, 40, 100));
        tab.setSpacing(10);
        tab.getChildren().addAll(new Label("Partie(s)"), this.tableauParties = new ListView<>());
        setTableauParties();
        this.tableauParties.setPrefWidth(200);
        this.tableauParties.setPrefHeight(150);
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(this.tableauParties);
        return tab;
    }

    public Scene classement() {
        BorderPane classement = new BorderPane();
        classement.setStyle("-fx-background-color: #ce8a70;");
        classement.setTop(retourHome());
        classement.setCenter(vGetvD());
        classement.setBottom(tableau());
        return new Scene(classement, 600, 400);
    }


}