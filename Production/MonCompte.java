import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Color;

public class MonCompte {
    private PageAcceuil main;
    private PageAcceuilJoueur acceuilJoueur;
    private  Utilisateur user;
    private TextField mail;
    private TextField identifiant;
    private ImageView img;
    public MonCompte(PageAcceuil main, PageAcceuilJoueur acceuilJoueur, Utilisateur user){
        this.main = main;
        this.acceuilJoueur = acceuilJoueur;
        this.user = user;
        this.mail = new TextField(this.user.getMail());
        this.identifiant = new TextField(this.user.getPseudo());
        this.img = null;
    }

    public Scene monCompte() {

        Label title = new Label("Mon Compte Personnel ");
        title.setFont(new Font(20));
        title.setAlignment(Pos.CENTER);
        VBox titleBox = new VBox();
        titleBox.getChildren().add(title);
        titleBox.setAlignment(Pos.CENTER);
        Label identifiant = new Label("Identifiant");
        identifiant.setFont(new Font(16));
        this.identifiant.setFont(new Font(16));
        Label mail = new Label("Email");
        mail.setFont(new Font(16));
        this.mail.setFont(new Font(16));
        this.img = new ImageView();
        img.setImage(this.user.getImage());
        img.setFitWidth(100) ;
        img.setPreserveRatio(true) ;

        Button bChoixFic=new Button("Choisir");
        bChoixFic.setOnAction(new ControleurChoisir(this.user, this,this.main));

        VBox boxImg = new VBox();
        boxImg.getChildren().addAll(img,bChoixFic);

        Button b = new Button("Scénarios Possibles");
        Button b2 = new Button("Statistiques");
        Button menu = new Button("Menu Principal");
        b.setFont(new Font(20));
        b2.setFont(new Font(20));
        menu.setFont(new Font(15));
        VBox bB = new VBox();
        VBox b2B = new VBox();
        VBox menuB = new VBox();
        menu.setOnAction(new MonCompteEvent(this.main,this.acceuilJoueur,this.user,this));
        b.setOnAction(new MonCompteEvent(this.main,this.acceuilJoueur,this.user,this));
        b2.setOnAction(new MonCompteEvent(this.main,this.acceuilJoueur,this.user,this));
        VBox menuBox = new VBox();
        menuBox.setAlignment(Pos.TOP_LEFT);
        menuB.getChildren().add(menu);
        menuB.setPadding(new Insets(5));
        bB.getChildren().add(b);
        bB.setPadding(new Insets(50));
        b2B.getChildren().add(b2);
        b2B.setPadding(new Insets(50));
        menuBox.getChildren().addAll(menuB);


        HBox idBox = new HBox();
        idBox.setAlignment(Pos.BASELINE_LEFT);
        VBox mailBox = new VBox();
        mailBox.setAlignment(Pos.BASELINE_LEFT);
        this.identifiant.setEditable(false);
        this.mail.setEditable(false);
        VBox idB = new VBox();
        idB.getChildren().add(this.identifiant);
        VBox mailB = new VBox();
        mailB.getChildren().add(this.mail);
        BorderPane bp = new BorderPane();
        idBox.getChildren().addAll(identifiant);
        mailBox.getChildren().addAll(mail);
        bp.setTop(menuBox);
        BorderPane bas = new BorderPane();

        menu.setStyle("-fx-background-color: #a06b56; ");
        b.setStyle("-fx-background-color: #a06b56; ");
        b2.setStyle("-fx-background-color: #a06b56; ");
        bas.setLeft(bB);
        bas.setRight(b2B);
        GridPane gp = new GridPane();

        idBox.setPadding(new Insets(5,5,5,15));
        mailBox.setPadding(new Insets(5,5,5,15));
        idB.setPadding(new Insets(5,5,5,15));
        mailB.setPadding(new Insets(5,5,5,15));

        gp.add(idBox,1,5);
        gp.add(boxImg,3,3);
        gp.add(idB,1,6);
        gp.add(mailBox,1,10);
        gp.add(mailB,1,11);
        gp.add(titleBox,2,2);

        bp.setBottom(bas);
        bp.setBackground(new Background(new BackgroundFill(Color.rgb(205   ,162,142),null,null)));
        bp.setCenter(gp);

        return new Scene(bp,this.main.largeurFondEcran,this.main.hauteurFondEcran);
    }
    public void setImage(ImageView img) {this.img = img;}
    public ImageView getImage() {return this.img;}
    public TextField getEmail() { return this.mail;}
    public TextField getIdentifiant(){return this.identifiant;}

}
