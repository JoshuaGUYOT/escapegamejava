import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StatStatistique {
    Connection connection;
    Statement st;

    public StatStatistique() {
        String login = "groupe31b";
        String mdp = "21@info!iuto31b";
        String serveur = "46.105.92.223";
        String bd = "db31b";
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver non trouvé");
            System.exit(1);
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + serveur + "/" + bd, login, mdp);
        } catch (
                SQLException ex) {
            System.out.println("Echec de connection " + ex.getMessage());
            System.exit(1);
        }
    }

    public String getNbParties(String pseudo) throws SQLException {

        st = connection.createStatement();
        ResultSet rs = st.executeQuery(
                "select count(idPa) nombrePartie from UTILISATEUR natural join PARTIE where pseudoUt =" + pseudo + ";");
        rs.next();
        int res = rs.getInt("nombrePartie");
        return "Nombre de parties jouées : " + res;
    }
    public int getNbPartiesAll() throws SQLException {

        st = connection.createStatement();
        ResultSet rs = st.executeQuery("select count(*) from PARTIE;");
        rs.next();
        return rs.getInt(1);
    }


    public String getTauxReussite(String joueur) throws SQLException {

        st = connection.createStatement();
        ResultSet rs = st.executeQuery( "select gagne, COUNT(idPa) as nbPartie from PARTIE natural join UTILISATEUR where pseudout ='"+joueur+"' group by gagne order by gagne;");

        int partiesG = 0;
        int partiesP = 0;
        double res;

        while (rs.next()) {
            if (rs.getString(1).equals("O")) {
                partiesG++;
            } else {
                partiesP++;
            }
        }
        if (partiesG == 0 && partiesP == 0){
            return "Aucune partie joué";
        }
        res = (partiesG * 100) / (partiesG + partiesP);
        return "Taux de réussite : " + res + " %";
    }


    public String getVictoireDefaite(String joueur) throws SQLException {

        st = connection.createStatement();
        ResultSet rs = st.executeQuery(
                "select gagne, count(idPa) nombreParties from PARTIE natural join UTILISATEUR where pseudout = '"+joueur+"' group by gagne order by gagne DESC;");

        int partiesG = 0;
        int partiesP = 0;

        while (rs.next()) {
            if (rs.getString(1).equals("O")) {
                partiesG += rs.getInt(2);
            } else {
                partiesP += rs.getInt(2);
            }
        }
        return "Victoire(s) : " + partiesG + "    Défaite(s) : " + partiesP;
    }
    public String getTauxReussiteScenar(Scenario scenario)  throws SQLException {
        st = connection.createStatement();
        ResultSet rs = st.executeQuery( "select gagne, COUNT(idPa) as nbPartie from PARTIE where idsc ='"+scenario.getIdScenario()+"' group by gagne order by gagne;");

        int partiesG = 0;
        int partiesP = 0;
        double res;

        while (rs.next()) {
            if (rs.getString(1).equals("O")) {
                partiesG++;
            } else {
                partiesP++;
            }
        }
        if (partiesG == 0 && partiesP == 0){
            return "Taux de réussite : aucune partie joué";
        }
        res = (partiesG * 100) / (partiesG + partiesP);
        return "Taux de réussite : " + res + " %";
    }

}