import javafx.scene.image.Image;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.sql.*;

public class AddImageJDBC {
    Connection connection;
    Statement st;
    Utilisateur user;

    public AddImageJDBC(Utilisateur user) {
        this.user = user;
        String login = "groupe31b";
        String mdp = "21@info!iuto31b";
        String serveur = "46.105.92.223";
        String bd = "db31b";
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver non trouvé");
            System.exit(1);
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + serveur + "/" + bd, login, mdp);
        } catch (
                SQLException ex) {
            System.out.println("Echec de connection " + ex.getMessage());
            System.exit(1);
        }
    }
    public void addImg(File file) throws Exception{
        PreparedStatement pre = connection.prepareStatement("UPDATE UTILISATEUR set avatarut=? where idut="+this.user.getIdUt());
            try {
                byte[] img = Files.readAllBytes(file.toPath());
                // création du BLOB qui stockera l'image
                Blob blob = connection.createBlob();
                // stockage de l'image dans le BLOB
                blob.setBytes(1, img);
                // affectation du BLOB dans la requête de mise à jour (le 1er ?)
                pre.setBlob(1, blob);
                // affectation de l'identifiant de la ligne qui va stocker le BLOB dans la BD
                // exécution de la requête de mise à jour
                int nb = pre.executeUpdate();
                GetUserJDBC getUser = new GetUserJDBC();
                this.user = getUser.refreshUser(user);
            } catch (SQLException throwables) {
                System.out.println("Problème SQL " + "\n" + throwables.getMessage());
            }
        }

}
