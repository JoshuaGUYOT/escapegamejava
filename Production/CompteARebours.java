import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.util.Duration;

/**
 * Permet de gérer un label associé à une Timeline pour afficher un temps écoulé
 */
public class CompteARebours extends Label{

    /*temps initial du compte à rebours en milisecondes*/
    private long temps;
    /*timeline qui va gérer le temps*/
    private Timeline timeline;
    /*la fenêtre de temps*/
    private KeyFrame keyFrame;
    /*le contrôleur associé au chronomètre*/
    private ActionTemps actionTemps;
    /*Accès à la vue du jeu de taquin (permettant notamment au controleur de désactiver le jeu*/
    private Game game;
    /*message à écrire devant le temps qui s'écoule*/
    private String message;
    /**
     * Constructeur permettant de créer le chronomètre
     * avec un label initialisé à la valeur du temps initial
     * Ce constructeur créer la Timeline, la KeyFrame et le contrôleur
     */
    CompteARebours(long tempsRestant, Game game, Placement placement, String message){
        super(message+" 0:0:0");
        this.temps = tempsRestant;
        this.message = message;
        this.game = game;
        this.actionTemps = new ActionTemps(this,this.game,placement,tempsRestant);
        this.keyFrame = new KeyFrame(Duration.millis(100),this.actionTemps);
        this.timeline = new Timeline(this.keyFrame);
        this.timeline.setCycleCount(( Animation.INDEFINITE));
    }
    public long getTime(){return this.temps;}
    /**
     * Permet au controleur de mettre à jour le label
     * la durée est affichée sous la forme h:m:s
     * @param tempsMillisec la durée depuis à afficher
     */
    public void setTime(long tempsMillisec){
        this.temps = tempsMillisec;

        int nbHeure = 0;
        int nbMinute = 0;
        int nbSeconde = 0;
        while(tempsMillisec>=3600000){
            tempsMillisec-=3600000;
            nbHeure ++;
        }
        while(tempsMillisec>=60000){
            tempsMillisec-=60000;
            nbMinute ++;
        }
        while(tempsMillisec>=1000){
            tempsMillisec-=1000;
            nbSeconde ++;
        }
        this.setText(this.message + nbHeure+":"+nbMinute+":"+nbSeconde);
    }

    /**
     * Permet de démarrer le compte à rebours
     */
    public void start(){
        this.timeline.play();
    }

    /**
     * Permet d'arrêter le compte à rebours
     */
    public void stop(){
        this.timeline.stop();
    }
    public void stopJust() {
        timeline.stop();
    }
    /**
     * Permet de remettre le compte à rebours au temps initial
     */
    public void resetTime(){
        this.actionTemps.reset(5000);
     }
}