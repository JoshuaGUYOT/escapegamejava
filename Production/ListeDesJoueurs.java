import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.paint.Color;

import static javafx.application.Application.launch;


public class ListeDesJoueurs{
    private PageAcceuilAdmin prec;
    private PageAcceuil  main;
    private Button boutonDesactiv, boutonActiv;
    public ListeDesJoueurs(PageAcceuilAdmin prec,PageAcceuil main) {
        this.main = main;
        this.prec = prec;
        this.boutonDesactiv = null;
        this.boutonActiv = null;
    }

    public Scene  getGererJoueur() {

        Button boutonRetour = new Button();
        Image img = new Image("file:./images/retour.png",20.0,0.0,true,true);
        boutonRetour.setGraphic(new ImageView(img));
        boutonRetour.setOnAction(new BackToAdminAcceuil(this.prec,this.main));

        BorderPane enTete = new BorderPane();
        enTete.setLeft(boutonRetour);
        Label lab2 = new Label("Liste des joueurs ");
        lab2.setFont(new Font(15));
        enTete.setCenter(lab2);
        BorderPane B = new BorderPane();
        lab2.setPadding( new Insets(10));
        B.setTop(enTete);

        HBox boxRecherche = new HBox();
        TextField texteF = new TextField();
        texteF.setMinWidth(500);
        Button boutonRecherche = new Button("valider");
        boxRecherche.getChildren().addAll(texteF,boutonRecherche);

        ScrollPane scrollPane = new ScrollPane();
        B.setCenter(scrollPane);
        scrollPane.setPadding(new Insets(5));

        List<BorderPane>  listeBoutons= new ArrayList<>();
        int i;
        List<Utilisateur> listeUser = null;
        try{
            AdminGereJDBC gerertools = new AdminGereJDBC();
            listeUser = gerertools.getListeJoueur();
        }
        catch (Exception e){
            System.out.println("Problème get liste user :"+e.getMessage());
        }
        for(i = 0; i<= listeUser.size()-1; i++){
            BorderPane bord  = new BorderPane();

            VBox boiteLab = new VBox();
            Label l = new Label(listeUser.get(i).getPseudo());
            l.setTextFill(Color.web("WHITE",0.8));
            boiteLab.getChildren().add(l);
            boiteLab.setAlignment(Pos.CENTER_LEFT);

            boiteLab.setPadding(new Insets(30));


            HBox boutons = new HBox();
            this.boutonActiv = new Button("Activer");
            this.boutonActiv.setOnAction(new ListeJoueurEvent(listeUser.get(i),this.main,this.prec));
            this.boutonDesactiv= new Button("Désactiver");
            this.boutonDesactiv.setOnAction(new ListeJoueurEvent(listeUser.get(i),this.main,this.prec));
            this.boutonActiv.setFont(new Font(13));
            this.boutonDesactiv.setFont(new Font(13));
            this.boutonActiv.setPrefWidth(this.boutonDesactiv.getPrefWidth());
            boutons.getChildren().addAll(this.boutonActiv,this.boutonDesactiv);
            boutons.setAlignment(Pos.CENTER_RIGHT);
            boutons.setSpacing(10);
            boutons.setStyle("-fx-padding: 20px; -fx-border-insets : 20px; -fx-background-insets:20px");
            bord.setRight(boutons);


            if (listeUser.get(i).isActiveUt()){
                this.boutonActiv.setDisable(true);
                bord.setStyle("-fx-background-color: #006400");
            }
            else {
                this.boutonDesactiv.setDisable(true);
                bord.setStyle("-fx-background-color: #8B0000");

            }

            bord.setLeft(boiteLab);
            bord.setMinWidth(500);
            bord.setMaxWidth(1500);
            listeBoutons.add(bord);
            scrollPane.setFitToWidth(true);
        }

        VBox boxx = new VBox(boxRecherche);
        boxx.setSpacing(5);
        boxx.getChildren().addAll(listeBoutons);

        BorderPane borderVbox = new BorderPane();
        borderVbox.getChildren().add(boxx);

        scrollPane.setContent(boxx);
        boxx.setAlignment(Pos.CENTER_LEFT);
        boxx.setSpacing(7);

        boxx.prefWidthProperty().bind(scrollPane.widthProperty());
        B.setBackground(new Background(new BackgroundFill(Color.rgb(205,162,142),null,null)));
        return new Scene(B, this.main.largeurFondEcran,this.main.hauteurFondEcran);
    }


}
