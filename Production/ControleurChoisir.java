import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;

import java.io.File;

class ControleurChoisir implements EventHandler<ActionEvent> {
    private Utilisateur user;
    private MonCompte account;
    private PageAcceuil main;
    public ControleurChoisir( Utilisateur user, MonCompte account, PageAcceuil main){
        this.user = user;
        this.account = account;
        this.main = main;
    }
    @Override
    public void handle(ActionEvent actionEvent) {
        FileChooser fc = new FileChooser();
        fc.setInitialDirectory(new File("./"));
        fc.setTitle("Choisis une image");
        FileChooser.ExtensionFilter ef = new FileChooser.ExtensionFilter("images","*.jpg","*.png");
        fc.getExtensionFilters().addAll(ef);
        fc.setSelectedExtensionFilter(ef);
        File fichier_choisi = fc.showOpenDialog(null);
        AddImageJDBC addImg = new AddImageJDBC(user);
        try {
            addImg.addImg(fichier_choisi);
            this.main.sceneChanger(this.account.monCompte());
        }
        catch (Exception e){
            Alert al =new Alert(Alert.AlertType.ERROR);
            al.setTitle("Image non-compatible");
            al.setHeaderText("Image non-compatible");
            al.showAndWait();
            System.out.println(e.getMessage());
        }

    }
}