import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class AcceuilEvent implements EventHandler<ActionEvent> {
    private PageAcceuil main;
    public AcceuilEvent(PageAcceuil primaryStage){
        this.main = primaryStage;
    }
    @Override
    public void handle(ActionEvent e) {
        Button b = (Button) e.getTarget();
        if (b.getText().endsWith("Se connecter")) {
            Connexion loginScreen = new Connexion(this.main);
            this.main.sceneChanger(loginScreen.connexionScreen());

        }
    }
}