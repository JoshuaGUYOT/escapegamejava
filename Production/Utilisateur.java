import javafx.scene.image.Image;

public class Utilisateur {
    private int idUt;
    private String mail;
    private String mdp;
    private String pseudo;
    private String role;
    private boolean activeUt;
    private Image avatar;
    public Utilisateur(int idUt, String mdp, String mail, String pseudo , Image avatar, String role, boolean activeUt){
        this.mail = mail;
        this.pseudo = pseudo;
        this.mdp = mdp;
        this.role = role;
        this.idUt = idUt;
        this.activeUt = activeUt;
        this.avatar = avatar;
    }
    public void setMdp(String mdp) {
        this.mdp = mdp;
    }
    public String getMdp() {
        return mdp;
    }
    public String getMail() {
        return mail;
    }
    public void setActiveUt(boolean activeUt) {
        this.activeUt = activeUt;
    }
    public boolean isActiveUt() {
        return activeUt;
    }
    public String getPseudo() {
        return pseudo;
    }
    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }
    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }
    public void setMail(String mail) {
        this.mail = mail;
    }
    public int getIdUt() {
        return idUt;
    }
    public void setIdentifiantUt(int ideUt) {
        this.idUt = idUt;
    }
    public Image getImage(){ return this.avatar; }
    public void setImage(Image image){this.avatar = image;}
}
