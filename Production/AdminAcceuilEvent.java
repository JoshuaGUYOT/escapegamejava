import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

public class AdminAcceuilEvent implements EventHandler<ActionEvent> {
    private PageAcceuil main;
    private PageAcceuilAdmin prec;
    public AdminAcceuilEvent(PageAcceuilAdmin prec,PageAcceuil main){
        this.main = main;
        this.prec = prec;
    }
    @Override
    public void handle(ActionEvent e) {
        Button b = (Button) e.getTarget();
        if (b.getText().endsWith("Gérer")) {
            ListeDesJoueurs gereJoueur = new ListeDesJoueurs(this.prec, this.main);
            this.main.sceneChanger(gereJoueur.getGererJoueur());
        }
        else {
            StatistiqueAdmin statAdmin = new StatistiqueAdmin(this.main,this.prec);
            this.main.sceneChanger(statAdmin.classement());
        }

    }
}
