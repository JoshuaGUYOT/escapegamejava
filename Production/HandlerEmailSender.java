import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;

public class HandlerEmailSender implements EventHandler<ActionEvent> {
    private MdpOublie main;
    private String bouton;
    public HandlerEmailSender(MdpOublie primaryStage, String bouton){
        this.main = primaryStage;
        this.bouton = bouton;
    }
    @Override
    public void handle(ActionEvent e) {
        if (this.bouton.equals("CONTINUER")) {
            if (this.main.getEmail().getText().length() != 0){
                main.setContinuerPopUp();
            }
            else {
                this.main.getEmail().requestFocus();
                Alert al =new Alert(Alert.AlertType.ERROR);
                al.setTitle("Attention !");
                al.setHeaderText("Pour envoyer un email, rentré une adresse email.");
                al.setContentText("Champ email vide.");
                al.showAndWait ();
            }
        }
        else{
            main.stop();
        }
    }
}