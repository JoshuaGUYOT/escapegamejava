import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;


public class PageAcceuilAdmin {
    private PageAcceuil main;
    private Utilisateur utilisateur;


    public int hauteurFondEcran = 500;
    public int largeurFondEcran = 600;
    public PageAcceuilAdmin(PageAcceuil main, Utilisateur utilisateur){
        this.main = main;
        this.utilisateur = utilisateur;
    }

    public Scene creerFormulaire(){
//        ----------[creation item]----------
        BorderPane acceuil = new BorderPane();
        VBox vBoxCentre = new VBox();
        VBox vboxTop = new VBox();
//        ---
        Label titre = new Label("Échappée Belle");
        titre.setFont(new Font("Times New Roman", 50));
        titre.setTextFill(Color.WHITE);
//        ---
        Button bGerer = new Button("Gérer");
        bGerer.setPrefSize(400, 25);
        bGerer.setStyle("-fx-background-color: #000000; ");
        bGerer.setTextFill(Color.WHITE);
        bGerer.setFont(new Font(15));
        bGerer.setPadding(new Insets(5));
        bGerer.setOnAction(new AdminAcceuilEvent(this,this.main));

        Button bStats = new Button("Statistiques");
        bStats.setPrefSize(400, 25);
        bStats.setStyle("-fx-background-color: #000000; ");
        bStats.setTextFill(Color.WHITE);
        bStats.setFont(new Font(15));
        bStats.setPadding(new Insets(5));
        bStats.setOnAction(new AdminAcceuilEvent(this,this.main));

        Button bQuitter = new Button("Quitter");
        bQuitter.setPrefSize(400, 25);
        bQuitter.setStyle("-fx-background-color: #000000; ");
        bQuitter.setTextFill(Color.WHITE);
        bQuitter.setFont(new Font(15));
        bQuitter.setPadding(new Insets(5));
        bQuitter.setOnAction(new QuitEvent(this.main));
//        ---
        Image fondEcran = new Image("./images/acceuilBackground.png",700, 0, true, true);
//        ----------[add item]----------
        vboxTop.getChildren().add(titre);
        vBoxCentre.getChildren().addAll(bGerer, bStats, bQuitter);
//        ---
        acceuil.setTop(vboxTop);
        acceuil.setCenter(vBoxCentre);
//        ----------[setting panel]----------
        acceuil.setBackground(new Background(new BackgroundImage(fondEcran,BackgroundRepeat.REPEAT,
                BackgroundRepeat.REPEAT,
                BackgroundPosition.DEFAULT,
                BackgroundSize.DEFAULT)));
        vboxTop.setAlignment(Pos.CENTER);
        vboxTop.setPadding(new Insets(50));
        vBoxCentre.setAlignment(Pos.BOTTOM_CENTER);
        vBoxCentre.setPadding(new Insets(50));
        vBoxCentre.setSpacing(15);
        return new Scene(acceuil, this.main.largeurFondEcran, this.main.hauteurFondEcran);
    }
    public Utilisateur getAdmin() {return this.utilisateur;}

}
