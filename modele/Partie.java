public class Partie {
    
    private Integer idPartie;
    private String heurePartie;
    private String datePartie;
    private String dureeLimite;
    private boolean resultat;

    public Partie(Integer idPartie, String heurePartie, String datePartie, String DureeLimite, boolean resultat){
        this.idPartie = idPartie;
        this.heurePartie = heurePartie;
        this.datePartie = datePartie;
        this.dureeLimite = dureeLimite;
        this.resultat = resultat;
    }

    public void setIdPartie(Integer idPartie) {
        this.idPartie = idPartie;
    }

    public void setHeurePartie(String heurePartie) {
        this.heurePartie = heurePartie;
    }

    public void setDatePartie(String datePartie) {
        this.datePartie = datePartie;
    }

    public void setDureeLimite(String dureeLimite) {
        this.dureeLimite = dureeLimite;
    }

    public Integer getIdPartie() {
        return idPartie;
    }

    public String getHeurePartie() {
        return heurePartie;
    }

    public String getDatePartie() {
        return datePartie;
    }

    public String getDureeLimite() {
        return dureeLimite;
    }

    public boolean isResultat() {
        return resultat;
    }

    public void setResultat(boolean resultat) {
        this.resultat = resultat;
    }
}
