import javafx.scene.image.Image;

public class Scenario {

    private int idScenario;
    private String nomScenario;
    private String resumeScenario;
    private Image iconeScenario;
    private int tempsMax;
    private String dateMiseEnligne;


    public Scenario(int idScenario, String nomScenario, String resumeScenario, Image iconeScenario, int tempsMax, String dateMiseEnligne){
        this.idScenario = idScenario;
        this.nomScenario = nomScenario;
        this.resumeScenario = resumeScenario;
        this.iconeScenario = iconeScenario;
        this.tempsMax = tempsMax;
        this.dateMiseEnligne =dateMiseEnligne;

    }

    public int getIdScenario() {
        return idScenario;
    }

    public String getNomScenario() {
        return nomScenario;
    }

    public String getResumeScenario() {
        return resumeScenario;
    }

    public Image getIconeScenario() {
        return iconeScenario;
    }

    public int getTempsMax() {
        return tempsMax;
    }

    public void setIdentifiantScenario(int idScenario) {
        this.idScenario = idScenario;
    }

    public void setNomScenario(String nomScenario) {
        this.nomScenario = nomScenario;
    }

    public void setResumeScenario(String resumeScenario) {
        this.resumeScenario = resumeScenario;
    }

    public void setIconeScenario(Image iconeScenario) {
        this.iconeScenario = iconeScenario;
    }

    public void setTempsMax(int tempsMax) {
        this.tempsMax = tempsMax;
    }

    public String getDateMiseEnligne() {
        return dateMiseEnligne;
    }

    public void setDateMiseEnligne(String dateMiseEnligne) {
        this.dateMiseEnligne = dateMiseEnligne;
    }
}
