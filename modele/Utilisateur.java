public class Utilisateur {

    private int idUt;
    private String mail;
    private String mdp;
    private String pseudo;
    private String etatUt;
    private String role;
    private boolean activeUt;




    public Utilisateur(int idUt, String mail, String pseudo, String etatUt, String mdp, String role, boolean activeUt){
        this.mail = mail;
        this.pseudo = pseudo;
        this.etatUt = etatUt;
        this.mdp = mdp;
        this.role = role;
        this.idUt = idUt;
        this.activeUt = activeUt;
    }



    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getMdp() {
        return mdp;
    }

    public String getMail() {
        return mail;
    }

    public void setActiveUt(boolean activeUt) {
        this.activeUt = activeUt;
    }

    public boolean isActiveUt() {
        return activeUt;
    }

    public String getEtatUt() {
        return etatUt;
    }

    public String getPseudo() {
        return pseudo;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public void setEtatUt(String etatUt) {
        this.etatUt = etatUt;
    }

    public void setPseudo(String pseudo) {
        this.pseudo = pseudo;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public int getIdUt() {
        return idUt;
    }

    public void setIdentifiantUt(int ideUt) {
        this.idUt = idUt;
    }
}
