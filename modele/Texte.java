import javafx.scene.image.Image;
import javafx.scene.media.MediaException;

public class Texte {
    private int idTxt;
    private String messageTxt;
    private Image imgTxt;
    private TypeTexte typeTxt;


    public Texte(int idTxt, String messageTxt, Image imgTxt, String typeTxt){
        this.idTxt = idTxt;
        this.messageTxt = messageTxt;
        this.imgTxt = imgTxt;

    }

    public int getIdTxt() {
        return idTxt;
    }

    public String getMessageTxt() {
        return messageTxt;
    }

    public Image getImgTxt() {
        return imgTxt;
    }


    public void setIdTxt(int idTxt) {
        this.idTxt = idTxt;
    }

    public void setMessageTxt(String messageTxt) {
        this.messageTxt = messageTxt;
    }

    public void setImgTxt(Image imgTxt) {
        this.imgTxt = imgTxt;
    }

}
