import javafx.scene.image.Image;

public class Enigme {

    private int idEn;
    private String nomEn;
    private String textEn;
    private String reponseEn;
    private Image imgEn;
    private String aideEn;
    private boolean brouillonEn;

    public int getIdEn() {
        return idEn;
    }

    public String getNomEn() {
        return nomEn;
    }

    public String getTextEn() {
        return textEn;
    }

    public String getReponseEn() {
        return reponseEn;
    }

    public Image getImgEn() {
        return imgEn;
    }

    public String getAideEn() {
        return aideEn;
    }

    public boolean isBrouillonEn() {
        return brouillonEn;
    }

    public void setIdEn(int idEn) {
        this.idEn = idEn;
    }

    public void setNomEn(String nomEn) {
        this.nomEn = nomEn;
    }

    public void setTextEn(String textEn) {
        this.textEn = textEn;
    }

    public void setReponseEn(String reponseEn) {
        this.reponseEn = reponseEn;
    }

    public void setImgEn(Image imgEn) {
        this.imgEn = imgEn;
    }

    public void setAideEn(String aideEn) {
        this.aideEn = aideEn;
    }

    public void setBrouillonEn(boolean brouillonEn) {
        this.brouillonEn = brouillonEn;
    }
}
