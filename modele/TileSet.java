import java.awt.*;

public class TileSet {

    private int idTileset;
    private String nomTileset;
    private Image imageTileset;

    public int getIdTileset() {
        return idTileset;
    }

    public String getNomTileset() {
        return nomTileset;
    }

    public Image getImageTileset() {
        return imageTileset;
    }

    public void setIdTileset(int idTileset) {
        this.idTileset = idTileset;
    }

    public void setNomTileset(String nomTileset) {
        this.nomTileset = nomTileset;
    }

    public void setImageTileset(Image imageTileset) {
        this.imageTileset = imageTileset;
    }
}
