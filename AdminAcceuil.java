import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import javax.naming.NamingEnumeration;

public class AdminAcceuil extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) {
        primaryStage.setTitle("Page d'accueil Administrateur");
        primaryStage.setScene(adminAcceuil());
        primaryStage.show();
    }

    private Scene adminAcceuil() {

        Label title = new Label("Informations");
        title.setFont(new Font(20));
        Button retour = new Button("Retour");
        Button menu = new Button("Menu Principal");
        Button stat = new Button("Consulter les Statistiques");
        Button gerer = new Button("Gérer les Joueurs");
        gerer.setStyle("-fx-background-color: #a06b56; ");
        stat.setStyle("-fx-background-color: #a06b56; ");

        gerer.setPrefWidth(400);
        gerer.setPrefHeight(60);
        gerer.setFont(new Font((20)));
        stat.setPrefHeight(60);
        stat.setPrefWidth(400);
        stat.setFont(new Font(20));

        title.setPadding(new Insets(0,120,0,120));
        HBox titleBox = new HBox();
        titleBox.setAlignment(Pos.CENTER);
        titleBox.getChildren().addAll(retour,title,menu);
        titleBox.setPadding(new Insets(40,0,0,0));
        VBox buttonBox = new VBox();
        buttonBox.setAlignment(Pos.CENTER);
        //gerer.setPadding(new Insets(20,0,20,0));
        //stat.setPadding(new Insets(20,0,20,0));
        VBox gererBox = new VBox();
        VBox statBox = new VBox();
        gererBox.setPadding(new Insets(30,0,30,0));
        statBox.setPadding(new Insets(20,0,0,0));
        gererBox.getChildren().addAll(gerer);
        statBox.getChildren().addAll(stat);
        statBox.setAlignment(Pos.CENTER);
        gererBox.setAlignment(Pos.CENTER);
        buttonBox.getChildren().addAll(gererBox,statBox);
        BorderPane bp = new BorderPane();
        bp.setTop(titleBox);
        bp.setCenter(buttonBox);

        bp.setBackground(new Background(new BackgroundFill(Color.rgb(205   ,162,142),null,null)));
        return new Scene(bp,600,500);
    }
}