import javafx.scene.image.Image;

public class Img extends Image {

    private static String chemin = "file:À définir";
    private String nomImage;
    private double taille;

    public Img(String nomImage, double taille){
//        Pour garder la taille initiale : taille = 0
        super(chemin+nomImage, taille, 0, true, true);
    }

    public Img(String nomImage){
        super(chemin+nomImage);
    }

    public String getChemin(){
        return chemin + nomImage;
    }
}
