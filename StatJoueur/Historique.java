import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class Historique {
    Connection connection;
    private int idJoueur;
    private List<Partie> historique;

    public Historique(int idJoueur){
        this.historique = new ArrayList<>();
        this.idJoueur = idJoueur;
        String login = "groupe31b";
        String mdp = "21@info!iuto31b";
        String serveur = "46.105.92.223";
        String bd = "db31b";
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver non trouvé");
            System.exit(1);
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + serveur + "/" + bd, login, mdp);
        } catch (
                SQLException ex) {
            System.out.println("Echec de connection " + ex.getMessage());
            System.exit(1);
        }
    }

    public List<Partie> MajHistorique() {
        try {
            boolean ajoute = false;
            Statement ps = connection.createStatement();
            ResultSet rs = ps.executeQuery("select idPa, dateDebutPa, gagne " +
                    "from PARTIE " +
                    "where idUt = " + idJoueur +
                    "order by dateDebutPa DESC");
            while (rs.next()) {
                int idPa = rs.getInt("idPa");
                Date datePartie = rs.getDate("dateDebutPa");
                String resultat = rs.getString("gagne");
                Partie p = new Partie(idJoueur, idPa, datePartie, resultat);
                if (!this.historique.contains(p)) {
                    this.historique.add(p);
                    ajoute = true;
                }
            }
            return this.historique;
        }catch (SQLException throwables) {
            System.out.println("Problème SQL \n" + throwables.getMessage());
        }
        return null;
    }

    public List<Partie> getHistorique(){
        return this.historique;
    }

}