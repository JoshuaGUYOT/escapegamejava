import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import javafx.scene.paint.Color;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;

public class Statistique extends Application {



    private ListView<HBox> tableauParties;
    private Label nombrePartiJouer;
    private Label tauxDeReussite;
    private Label victoirDefait;
    private Button retour;
    private Button  home;
    private Historique historique;
    private Partie partie;

    this.historique = new Historique();






    public void setTableauParties() {

        this.historique = new Historique(idjoueur);
        for (Partie x: this.historique.MajHistorique()) {
            if (x.isResultat() == "1") {
                HBox y = new HBox();
                y.getChildren().addAll(new Label("" + y.getIdPartie()));
                y.setStyle("-fx-background-color: #b9fd9e; ");
                tableauParties.getItems().add(y);
            } else {
                HBox x = new HBox();
                x.getChildren().addAll(new Label("" + x.getIdPartie()));
                x.setStyle("-fx-background-color: #fd9e9e; ");
                tableauParties.getItems().add(x);
            }
        }
    }

    public static void main(String[] args) {
        launch(args);
    }


    public BorderPane retourHome(){
        BorderPane bpEnTete = new BorderPane();
        bpEnTete.setStyle("-fx-background-color: #a06b56; ");
        Image img = new Image("file:./home.jpg",50,15,true,true);
        ImageView view = new ImageView(img);
        Image img2 = new Image("file:./retour.png",50,15,true,true);
        ImageView view2 = new ImageView(img2);
        this.retour = new Button();
        this.retour.setGraphic(view2);
        //this.retour.setOnAction(new BoutonStatique(this));
        this.retour.setPrefWidth(75);
        this.home = new Button();
        this.home.setGraphic(view);
        this.home.setAlignment(Pos.CENTER_RIGHT);
        //this.home.setOnAction(new BoutonStatique(this));
        this.home.setPrefWidth(75);
        Label l= new Label("Statistique");
        l.setAlignment(Pos.CENTER);
        l.setStyle("-fx-font-weight: bold");
        l.setFont(new Font(l.getFont().getName(),20));
        l.setPrefWidth(130);
        bpEnTete.setLeft(this.retour);
        bpEnTete.setRight(this.home);
        bpEnTete.setCenter(l);
        return bpEnTete;
    }


    public VBox nbParties(){
        VBox vG = new VBox();
        vG.setStyle("-fx-background-color: #ce8a70;");
        vG.setPrefWidth(275);
        vG.setPrefHeight(150);
        vG.setSpacing(10);
        this.nombrePartiJouer = new Label("getNbParties(joueur)");
        vG.getChildren().addAll(this.nombrePartiJouer);
        return vG;
    }

    public VBox tauxReussite(){
        VBox vD = new VBox();
        vD.setAlignment(Pos.TOP_RIGHT);
        vD.setStyle("-fx-background-color: #ce8a70;");
        vD.setPrefWidth(275);
        vD.setPrefHeight(150);
        vD.setSpacing(10);
        this.tauxDeReussite = new Label("getTauxReussite(joueur)");
        this.victoirDefait = new Label("getVictoireDefaite(joueur)");
        vD.getChildren().addAll(this.tauxDeReussite, this.victoirDefait );
        return vD;
    }


    public BorderPane vGetvD(){
        BorderPane h = new BorderPane();
        h.setStyle("-fx-background-color: #ce8a70;");
        h.setPadding(new Insets(20, 20, 20,20 ));

        h.setRight(tauxReussite());
        h.setLeft(nbParties());
        return h;
    }



    public VBox tableau() {
        VBox tab= new VBox();
        //tab.setStyle("-fx-background-color: #ce8a70;");
        tab.setAlignment(Pos.CENTER);
        tab.setPadding(new Insets(0, 100, 40, 100));
        tab.setSpacing(10);
        tab.getChildren().addAll(new Label("Partis"), this.tableauParties = new ListView<>());
        setTableauParties();
        this.tableauParties.setPrefWidth(200);
        this.tableauParties.setPrefHeight(150);
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.setContent(this.tableauParties);
        return tab;
    }

    public Scene classement() {
        BorderPane classement = new BorderPane();
        classement.setStyle("-fx-background-color: #ce8a70;");
        classement.setTop(retourHome());
        classement.setCenter(vGetvD());
        classement.setBottom(tableau());
        return new Scene(classement, 600, 400);
    }


    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Statistique");
        primaryStage.setScene(classement());
        primaryStage.show();
    }

}