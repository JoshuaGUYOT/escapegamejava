import java.sql.*;
import java.util.List;

public class JoueurBD{
    private ConnexionMySQL laConnexion;

    public JoueurBD(ConnexionMySQL laConnexion){
        this.laConnexion=laConnexion;
    }


    public List<Utilisateur> getListeJoueurs()throws SQLException{


        statement s = createStatament();
        ResultSet rs = s.executeQuery("
            select pseudoUt, activeUt, nomRole
            from UTILISATEUR natural join ROLE
            where nomRole = 'joueur';"
        )

        List<Utilisateur> res = new ArrayList<>();

        while (rs.next(){
            if (rs.getString("nomRole") == joueur){
                List<String> liste = new ArrayList<>();

                liste.add(rs.getString("pseudoUt"));
                liste.add(rs.getString("activeUt"))

                res.add(liste);
            }
        }
        return res;
    }





    public Utilisateur getJoueur(Strign joueur)throws SQLException{

        Statement st= this.laConnexion.CreateStatement();
        ResultSet rs= st.executeQuery("select pseudoUt, activeUt from UTILISATEUR where pseudoUt =" joueur);
        
        if (rs.next()){
            List<String> res = new ArrayList<>();

            res.add(rs.getString("pseudoUt"));
            if (rs.getChar("activeUt" == 1)){
                res.add("active");
            }
            else{
                res.add("non_activee");
            }
            return res;
        else{
            throws new SQLException("Joueur " +joueur+ "n'existe pas");
        }
            
        }
    
    }



    public String getNbParties( String joueur) throws SQLException{

        statement s = createStatament();
        ResultSet rs = s.executeQuery(
            "select count(idPa) nombrePartie
            from UTILISATEUR natural join PARTIE
            where pseudoUt ="joueur );

        int res = rs.getInt("nombrePartie");
        return "Nombre de parties jouées : " +res;
    }


    public String getTauxReussite(String joueur){

        statement s = createStatament();
        Resultset rs = s.executeQuery(
            "select gagne, count(idPa) nombreParties
            from PARTIE natural join UTILISATEUR
            groupe by gagne
            ordre by gagne = 1
            where pseudo" 
        );

        int partiesG;
        int partiesP;
        double res;

        while (rs.next()){
            if (rs.getChar("gagne") == 1){
                partiesG = rs.getInt("nombreParties");
            }
            else{
                partiesP = rs.getInt("nombreParties");
            }
        }
        res = partiesG * 100 / partiesG + partiesP;
        return "Taux de réussite : "+res+" %";
    }




    public String getVictoireDefaite(String joueur){

        statement s = createStatament();
        Resultset rs = s.executeQuery(
            "select gagne, count(idPa) nombreParties
            from PARTIE natural join UTILISATEUR
            groupe by gagne
            ordre by gagne DESC
            where pseudo ="+joueur);

        int partiesG;
        int partiesP;

        while (rs.next()){
            if (rs.getChar("gagne") == 1){
                partiesG = rs.getInt("nombreParties");
            }
            else{
                partiesP = rs.getInt("nombreParties");
            }
        }
        return "Victoire : "+partiesG+"    Défaite : "+partiesP;
    }



    public List<List> getListeParties(String joueur) throws SQLException{


        statement s = createStatament();
        ResultSet rs = s.executeQuery("
        select idPa, gagne
        from UTILISATEUR natural join PARTIE
        where pseudoUt =" joueur );

        List<List> res = new ArrayList<>();

        while(rs.next()){
            List<String> liste = new ArrayList<>();

            liste.add(rs.getString("idPa"));
            if (rs.getChar("gagne") == 1){
                liste.add("Gagné");
            }
            else{
                liste.add("Perdu")
            }
            res.add(liste);
        return res;
        }
    }

}