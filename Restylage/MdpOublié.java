import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.NodeOrientation;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.text.Font;

import java.awt.desktop.ScreenSleepEvent;
import java.util.concurrent.ConcurrentNavigableMap;

public class MdpOublié{

    private TextField email;
    private PageAcceuil main;

    public MdpOublié(PageAcceuil prec) {
        this.email = null;
        this.main = prec;
    }
    public Scene mdpForget() {


        BorderPane boiteTotal = new BorderPane();

        Label title = new Label("MOT DE PASSE OUBLIE ");
        title.setStyle("-fx-font-weight: bold");



        HBox titleBox = new HBox();
        titleBox.setAlignment(Pos.CENTER);
        titleBox.getChildren().addAll(title);
        titleBox.setStyle("-fx-background-color:#CCA28E");
        titleBox.setPadding(new Insets(50));
        title.setFont(new Font(title.getFont().getName(),17));


        Button b2 = new Button();
        Image img1 = new Image("./images/Retour.png",30,0,true,true);
        b2.setGraphic(new ImageView(img1));
        b2.setGraphic(new ImageView(img1));
        b2.setOnAction(new HandlerEmailSender(this));
        b2.setStyle("-fx-background-color:#CCA28E");


        Button b = new Button();
        Image img = new Image("./images/done.png",30,0,true,true);
        b.setGraphic(new ImageView(img));
        b.setStyle("-fx-background-color:#CCA28E");
        b.setOnAction(new HandlerEmailSender(this));



        Label texte = new Label("Si vous avez oublié votre mot de passe, veuillez entrer votre adresse e-mail enregistrée.\n" +
                "Nous vous enverrons un lien pour réinitialiser votre mot de passe.");




        VBox texteBox = new VBox();
        texteBox.setAlignment(Pos.CENTER);
        texteBox.setStyle("-fx-background-color:#CCA28E");
        texteBox.getChildren().add(texte);




        this.email = new TextField();
        BorderPane a = new BorderPane();
        GridPane p = new GridPane();

        a.setTop(titleBox);
        a.setPadding(new Insets(20,20,20,30));



        VBox centerBox = new VBox();
        p.setAlignment(Pos.CENTER);
        centerBox.getChildren().addAll(titleBox,texteBox,this.email);

        FlowPane buttonBox = new FlowPane();
        buttonBox.getChildren().addAll(b2,b);
        p.add(centerBox,1,1);
        buttonBox.setAlignment(Pos.BASELINE_RIGHT);
        buttonBox.setPadding(new Insets(10));
        buttonBox.setStyle("-fx-background-color:#CCA28E");
        buttonBox.setAlignment(Pos.BOTTOM_CENTER);
        p.add(buttonBox,1,2);


        a.setStyle("-fx-background-color:#CCA28E");
        a.setCenter(p);

        texteBox.setPadding(new Insets(10));
        b.setPadding(new Insets(10));
        b2.setPadding(new Insets(10));

        email.setStyle("-fx-background-color:#FFDAD0");

        return new Scene(a,this.main.largeurFondEcran, this.main.hauteurFondEcran);
    }


    public TextField getEmail() { return this.email;}

    public void setContinuerPopUp(){
        final Stage dialog = new Stage();
        dialog.setTitle("MOT DE PASSE OUBLIÉ : E-MAIL ENVOYÉ");
        dialog.initModality(Modality.APPLICATION_MODAL);
        GridPane dialogVbox = new GridPane();
        Text title = new Text("MOT DE PASSE OUBLIÉ : E-MAIL ENVOYÉ");
        VBox titleB = new VBox();
        titleB.getChildren().add(title);
        titleB.setPadding(new Insets(10));
        titleB.setAlignment(Pos.CENTER);
        dialogVbox.add(titleB,0,1);
        dialogVbox.add(new Text("Un e-mail a été envoyé à l'adresse e-mail fournie.\n" +
                "\n" +
                "    Si vous avez demandé un nouveau mot de passe mais n'avez pas reçu un e-mail de réinitialisation de mot de passe:\n" +
                "\n" +
                "    1. Vérifiez le dossier spam.\n" +
                "    2.  Réinitialisez votre mot de passe encore une fois.\n" +
                "    3. Si vous ne recevez toujours pas l'e-mail après avoir demandé une réinitialisation de mot de passe, essayez à nouveau après 24h. \n" +
                "\n"),0,2);
        Button leave = new Button("Retour");
        dialogVbox.add(leave,2,3);
        leave.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                dialog.hide();
            }
        });
        Scene dialogScene = new Scene(dialogVbox, 1000, 250);
        dialog.setScene(dialogScene);

        dialog.show();
    }
    public void stop(){
       Connexion co = new Connexion(this.main);
        this.main.sceneChanger(co.connexionScreen());
    }

}
