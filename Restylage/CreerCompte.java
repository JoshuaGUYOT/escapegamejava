import javafx.scene.paint.Color;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;



public class CreerCompte {
    private VBox root;
    private TextField id;
    private PasswordField mdp;
    private PasswordField confMdp;
    private TextField email;
    private PageAcceuil main;
    private Connexion connexion;
    public CreerCompte(PageAcceuil main, Connexion connexion){
        this.main = main;
        this.root = new VBox();
        this.connexion = connexion;
        this.id = new TextField();
        this.mdp = new PasswordField();;
        this.email = new TextField();
        this.confMdp = new PasswordField();
    }

    public Scene getRegister() {
        VBox vBox = new VBox();
        vBox.setPadding(new Insets(20,20,20,20));
        vBox.setSpacing(8);
        root.setStyle("-fx-background-color: #CCA28E");

        Button back = new Button();
        back.setMinWidth(30);
        Image img = new Image("./images/Retour.png",30,0,true,true);
        back.setStyle("-fx-background-color: transparent");
        //back.setBackground(new Background(new BackgroundFill(Color.rgb(220,180,170),null,null)));
        back.setGraphic(new ImageView(img));
        back.setOnAction(new CreerCompteEvent(this.connexion,this.main,this,"Retour"));

        id.setStyle("-fx-control-inner-background : #FFDAD0 ");
        mdp.setStyle("-fx-control-inner-background : #FFDAD0 ");
        email.setStyle("-fx-control-inner-background : #FFDAD0 ");
        confMdp.setStyle("-fx-control-inner-background : #FFDAD0 ");


        VBox titleB = new VBox();
        Label title = new Label("Creer un compte");


        Button valider = new Button();
        Image img1 = new Image("./images/done.png",30,0,true,true);
        valider.setGraphic(new ImageView(img1));
        valider.setPadding(new Insets(0,25,0,10));
        valider.setStyle("-fx-background-color: #CCA28E");
        valider.setOnAction(new CreerCompteEvent(this.connexion,this.main,this,"Valider"));


        title.setFont(new Font(title.getFont().getName(),20));
        title.setStyle("-fx-font-weight: bold");
        titleB.setAlignment(Pos.CENTER);
        titleB.setPadding( new Insets(10));
        titleB.getChildren().add(title);

        VBox boutonValider = new VBox();
        boutonValider.getChildren().add(valider);
        boutonValider.setAlignment(Pos.BOTTOM_RIGHT);


        vBox.getChildren().addAll(back,
                titleB,
                new Label("Identifiant"),
                this.id,
                new Label("Email"),
                this.email,
                new Label("Mot de passe"),
                this.mdp,
                new Label("Confirmer le mot de passe"),
                this.confMdp);
        root.getChildren().addAll(vBox,boutonValider);
        

        return new Scene(root,this.main.largeurFondEcran,this.main.hauteurFondEcran);

    }

    public PasswordField getMDP(){return this.mdp;}
    public TextField getLogin() {return this.id;}
    public PasswordField getMDP2() {return this.confMdp;}
    public TextField getEmail() {return this.email;}
}
