import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class CarteJDBC{
    Connection connection;
    private int idca;
    private int idts;

    public CarteJDBC(int idca,int idts){
        this.idca = idca;
        this.idts=idts;

        String login = "groupe31b";
        String mdp = "21@info!iuto31b";
        String serveur = "46.105.92.223";
        String bd = "db31b";
        try {
            Class.forName("org.mariadb.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            System.out.println("Driver non trouvé");
            System.exit(1);
        }
        try {
            connection = DriverManager.getConnection("jdbc:mysql://" + serveur + "/" + bd, login, mdp);
        } catch (
                SQLException ex) {
            System.out.println("Echec de connection " + ex.getMessage());
            System.exit(1);
        }
    }

    public String getPlanC()throws SQLException{
        try{
            Statement s = connection.createStatement();
            ResultSet rs = s.executeQuery("select * from CARTE where idca = "+idca);

            return rs.getString("*");
        }
        catch (SQLException throwables) {
            System.out.println("Problème SQL \n" + throwables.getMessage());
        }
        return null;
    }

    public String getTuile() throws SQLException{
        try{
            Statement ss = connection.createStatement();
            ResultSet rss = ss.executeQuery("select * from TILESET where idts ="+idts);

            return rss.getString("*");
        }
        catch (SQLException throwables){
            System.out.println("Problème SQL \n"+throwables.getMessage());
        }
        return null;
    }

}
