import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.paint.Color;

import static javafx.application.Application.launch;


public class ListeDesJoueurs extends Application{
    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) {
        // Create a ScrollPane

        Button boutonRetour = new Button();
        Image img = new Image("file:/home/lacan/images/Retour.png",20.0,0.0,true,true);
        boutonRetour.setGraphic(new ImageView(img));
        Button boutonAcceuil = new Button();
        Image img1 = new Image("file:/home/lacan/images/Acceuil.png",20.0,0.0,true,true);
        boutonAcceuil.setGraphic(new ImageView(img1));

        BorderPane enTete = new BorderPane();
        enTete.setLeft(boutonRetour);
        Label lab2 = new Label("Liste des joueurs ");
        enTete.setCenter(lab2);
        enTete.setRight(boutonAcceuil);
        BorderPane B = new BorderPane();
        lab2.setPadding( new Insets(10));
        B.setTop(enTete);



        HBox boxRecherche = new HBox();
        TextField texteF = new TextField();
        texteF.setMinWidth(500);
        Button boutonRecherche = new Button("valider");
        boxRecherche.getChildren().addAll(texteF,boutonRecherche);


        ScrollPane scrollPane = new ScrollPane();
        B.setCenter(scrollPane);
        scrollPane.setPadding(new Insets(5));


        // Set content for ScrollPane
        //scrollPane.setContent();

        List<BorderPane>  listeBoutons= new ArrayList<>();
        int i;
        for(i = 0; i<=25; i++){
            BorderPane bord  = new BorderPane();

            //bord.setPadding(20);
            if(i%2==0){bord.setStyle("-fx-background-color: #CCA28E");}
            else if(i%2 !=0){bord.setStyle("-fx-background-color: #6496FA");}


            VBox boiteLab = new VBox();
            Label l = new Label("" + (i+1) );
            boiteLab.getChildren().add(l);
            boiteLab.setAlignment(Pos.CENTER_LEFT);

            boiteLab.setPadding(new Insets(30));


            HBox boutons = new HBox();
            Button boutonActiv = new Button("Activer");
            //boutonActiv.setPadding(new Insets(0,10,0,10));
            Button boutonDesactiv = new Button("Désactiver");
            //boutonDesactiv.setPadding(new Insets(0,10,0,10));
            boutonActiv.setFont(new Font(13));
            boutonDesactiv.setFont(new Font(13));
            boutonActiv.setPrefWidth(boutonDesactiv.getPrefWidth());
            boutons.getChildren().addAll(boutonActiv,boutonDesactiv);
            boutons.setAlignment(Pos.CENTER_RIGHT);
            boutons.setSpacing(10);
//            HBox.setMargin(boutons, new Insets(20));
            boutons.setStyle("-fx-padding: 20px; -fx-border-insets : 20px; -fx-background-insets:20px");
            bord.setRight(boutons);

            bord.setLeft(boiteLab);
            bord.setMinWidth(500);
            bord.setMaxWidth(1500);
            listeBoutons.add(bord);
            scrollPane.setFitToWidth(true);

            bord.setOnMousePressed(k->{
                bord.setStyle("-fx-background-color: #6496FA");
                System.out.println("sfs");
            });
            bord.setOnMouseReleased(j -> bord.setStyle("-fx-background-color: #DCB4AA"));
        }







        //HBox.setHgrow(b12, Priority.ALWAYS);
        //HBox.setHgrow(lab, Priority.ALWAYS);
        //h.setOnMouseClicked(variable->{
        //    System.out.println("snhvsn");
        // });
        VBox boxx = new VBox(boxRecherche);
        boxx.setSpacing(5);
        boxx.getChildren().addAll(listeBoutons);

        BorderPane borderVbox = new BorderPane();
        borderVbox.getChildren().add(boxx);
        // borderVbox.setM

        scrollPane.setContent(boxx);
        boxx.setAlignment(Pos.CENTER_LEFT);
        boxx.setSpacing(7);

        boxx.prefWidthProperty().bind(scrollPane.widthProperty());
//        VBox.setMargin(boxx,new Insets(10,10,10,10));
        // Always show vertical scroll bar
        B.setBackground(new Background(new BackgroundFill(Color.rgb(205,162,142),null,null)));
        primaryStage.setTitle("Liste des joueurs");
        Scene scene = new Scene(B, 700, 400);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


}
