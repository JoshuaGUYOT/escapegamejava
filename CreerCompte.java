import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.awt.*;

public class CreerCompte extends Application  {
    private StackPane root = new StackPane();
    private Stage stage;

    @Override
    public void init() {
        Label title = new Label("Créer un compte ");
        Button button = new Button("OPEN");
        VBox vBox = new VBox();
        vBox.setPadding(new Insets(20,20,20,20));
        vBox.setSpacing(8);
        Label l = new Label("identifiant");
        vBox.getChildren().addAll(l,
                new TextField(),
                new Label("Email"),
                new TextField(),
                new Label("Mot de passe"),
                new PasswordField(),
                new Label("Confirmer le mot de passe"),
                new PasswordField(),
                new Button("Valider"));
        root.getChildren().addAll(vBox);

        button.setOnAction(actionEvent-> {
            if(stage!=null){
                stage.requestFocus();
                return;
            }

            stage = new Stage();
            StackPane stackPane = new StackPane();
            stage.setScene(new Scene(stackPane, 700,400));
            stage.show();
        });
    }

    @Override
    public void start(Stage primaryStage) {
        Scene scene = new Scene(root,700,400);
        primaryStage.setScene(scene);
        primaryStage.show();
        primaryStage.setTitle("Créer un compte");
        primaryStage.setAlwaysOnTop(true);
    }


    public static void main(String[] args) {
        launch(args);
    }
}
