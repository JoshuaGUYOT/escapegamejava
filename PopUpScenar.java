import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

import javax.naming.NamingEnumeration;
import javax.security.auth.callback.LanguageCallback;

public class PopUpScenar extends Application {
    public static void main(String[] args) {
        launch(args);
    }

    public void start(Stage primaryStage) {
        primaryStage.setTitle("Page d'accueil Administrateur");
        primaryStage.setScene(popUpScenar());
        primaryStage.show();
    }

    private Scene popUpScenar() {

        Label title = new Label("Nom scénario");
        Label timer = new Label("Temps : 00.00.00");
        Label desc = new Label("Description du scenar");
        Button jouer = new Button("Jouer");

        HBox titleBox = new HBox();
        title.setPadding(new Insets(0,360,0,0));
        titleBox.setAlignment(Pos.CENTER);
        titleBox.getChildren().addAll(title,timer);
        titleBox.setPadding(new Insets(10,0,0,0));

        jouer.setStyle("-fx-background-color: #a06b56; ");
        HBox buttonBox = new HBox();
        jouer.setFont(new Font(14));
        buttonBox.setAlignment(Pos.CENTER_RIGHT);
        buttonBox.getChildren().addAll(jouer);
        buttonBox.setPadding(new Insets(0,5,5,0));

        VBox textBox = new VBox();
        textBox.getChildren().addAll(desc);
        textBox.setPadding(new Insets(10,0,0,20));

        BorderPane bp = new BorderPane();
        bp.setCenter(textBox);
        bp.setBottom(buttonBox);
        bp.setTop(titleBox);
        bp.setBackground(new Background(new BackgroundFill(Color.rgb(205,162,142),null,null)));
        return new Scene(bp,600,180);
    }
}